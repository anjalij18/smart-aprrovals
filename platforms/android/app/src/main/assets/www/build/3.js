webpackJsonp([3],{

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoListPageModule", function() { return PoListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__po_list__ = __webpack_require__(366);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PoListPageModule = /** @class */ (function () {
    function PoListPageModule() {
    }
    PoListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__po_list__["a" /* PoListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__po_list__["a" /* PoListPage */]),
            ],
        })
    ], PoListPageModule);
    return PoListPageModule;
}());

//# sourceMappingURL=po-list.module.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PoListPage = /** @class */ (function () {
    function PoListPage(navCtrl, navParams, url, alertCtrl, httpSoap, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.url = url;
        this.alertCtrl = alertCtrl;
        this.httpSoap = httpSoap;
        this.toastCtrl = toastCtrl;
        this.obj = [];
        this.objSearch = [];
        this.userId = navParams.get("param");
    }
    PoListPage_1 = PoListPage;
    PoListPage.prototype.ionViewDidLoad = function () { };
    PoListPage.prototype.ionViewDidEnter = function () { };
    PoListPage.prototype.ngOnInit = function () {
        var that = this;
        if (that.url.loading) {
            that.url.stopLoading();
        }
        this.url.startLoading().present();
        this.url.checkSession(function (err, resp) {
            if (resp) {
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                that.getPOlist();
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PoListPage.prototype.doRefresh = function (refresher) {
        this.ngOnInit();
        setTimeout(function () {
            refresher.complete();
        }, 200);
    };
    PoListPage.prototype.initializeItems = function () {
        this.objSearch = this.obj;
    };
    PoListPage.prototype.getItems = function (ev) {
        this.initializeItems();
        var that = this;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            that.objSearch = that.obj.filter(function (item) {
                return (item.old[0].STL_TRA_PORELEASE[0].por_ponumber.toString().toLowerCase().indexOf(val.toLowerCase()) > -1 || item.old[0].STL_TRA_PORELEASE[0].por_vendor_name.toString().toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            console.log("search====", that.objSearch);
        }
    };
    PoListPage.prototype.getPOlist = function () {
        var method = 'GetListOfPOsPending';
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_3_jsencrypt__["JSEncrypt"]();
        var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        that.userId = '';
        that.userId = encrUser;
        that.getPOsubCode(parameters, method, that);
    };
    PoListPage.userPOcount = function (username, tk) {
        var parameters = [];
        parameters["userId"] = username;
        parameters["tk"] = tk;
        return parameters;
    };
    PoListPage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    PoListPage.prototype.poDetails = function (data) {
        this.navCtrl.push('PoDetailsPage', {
            param: data
        });
    };
    PoListPage.prototype.getPOsubCode = function (parameters, method, that) {
        parameters['GetListOfPOsPending xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PoListPage_1.userPOcount(that.userId, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // that.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.url.stopLoading();
                var temp = response;
                var s = temp['SOAP:Envelope'];
                try {
                    var a = s['SOAP:Body'][0].GetListOfPOsPendingResponse;
                    var b = a[0].tuple;
                    if (b == undefined) {
                        var alert_1 = that.alertCtrl.create({
                            message: "No data found..!",
                            buttons: [{
                                    text: 'OK',
                                    handler: function () {
                                        that.navCtrl.setRoot("HomePage");
                                    }
                                }]
                        });
                        alert_1.present();
                    }
                    else {
                        that.obj = b;
                        that.objSearch = b;
                        var formatter = new Intl.NumberFormat('en-US', {
                            minimumFractionDigits: 2,
                        });
                        for (var i = 0; i < that.objSearch.length; i++) {
                            that.objSearch[i].old[0].STL_TRA_PORELEASE[0].por_netvalue = formatter.format(that.objSearch[i].old[0].STL_TRA_PORELEASE[0].por_netvalue); /* $2,500.00 */
                        }
                    }
                }
                catch (e) {
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    var toast = that.toastCtrl.create({
                        message: "Token expired!! Please refresh once..",
                        duration: 2500,
                        position: "top"
                    });
                    toast.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PoListPage = PoListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-po-list',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/po-list/po-list.html"*/'<ion-header>\n\n  <ion-navbar color="sterlite">\n    <ion-title>\n      <div style="letter-spacing: 2px;">Purchase Orders</div>\n    </ion-title>\n  </ion-navbar>\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n</ion-header>\n\n<ion-content class="mainContent">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-list>\n    <ion-item *ngFor="let o of objSearch" (tap)="poDetails(o)">\n      <ion-row>\n        <ion-col col-6>\n\n          <h2>PO # {{o.old[0].STL_TRA_PORELEASE[0].por_ponumber}}</h2>\n\n          <h4 style="margin-top: 5%;" *ngIf="!o.old[0].STL_TRA_PORELEASE[0].por_vendor_name[0].$">\n            {{o.old[0].STL_TRA_PORELEASE[0].por_vendor_name}}</h4>\n        </ion-col>\n        <ion-col col-6 style="text-align: right">\n          <!-- <h2>{{p.old[0].PurchaseRequisitionRelease[0].LINECOUNT}} </h2> -->\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12 style="text-align: right">\n          <p>\n            <span style="font-size: 16px; color: black;"\n              *ngIf="!o.old[0].STL_TRA_PORELEASE[0].por_currency[0].$">{{o.old[0].STL_TRA_PORELEASE[0].por_currency}}</span>&nbsp;\n            <span style="font-size: 18px; color: black;"\n              *ngIf="o.old[0].STL_TRA_PORELEASE[0].por_netvalue!=\'NaN\'">{{o.old[0].STL_TRA_PORELEASE[0].por_netvalue}}</span>\n            <span style="font-size: 18px; color: black;"\n              *ngIf="o.old[0].STL_TRA_PORELEASE[0].por_netvalue==\'NaN\' ">0</span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n</ion-content>\n<ion-footer class="paraHead">\n  Sterlite Power\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/po-list/po-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_http_soap_http_soap__["a" /* HttpSoapProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], PoListPage);
    return PoListPage;
    var PoListPage_1;
}());

//# sourceMappingURL=po-list.js.map

/***/ })

});
//# sourceMappingURL=3.js.map