webpackJsonp([5],{

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtPpagePageModule", function() { return OtPpagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ot_ppage__ = __webpack_require__(364);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { SoapService } from '../../providers/soap-service/soap-service';
var OtPpagePageModule = /** @class */ (function () {
    function OtPpagePageModule() {
    }
    OtPpagePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ot_ppage__["a" /* OtPpagePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__ot_ppage__["a" /* OtPpagePage */]),
            ],
            providers: [],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], OtPpagePageModule);
    return OtPpagePageModule;
}());

//# sourceMappingURL=ot-ppage.module.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtPpagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OtPpagePage = /** @class */ (function () {
    function OtPpagePage(url, navCtrl, navParams, formBuilder, events, alertCtrl, toastCtrl, httpSoap) {
        this.url = url;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.httpSoap = httpSoap;
        this.isUnchanged = false;
        this.signinObj = localStorage.getItem('userdetail');
        this.OTPForm = this.formBuilder.group({
            otp1: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            otp2: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            otp3: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            otp4: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
    }
    OtPpagePage_1 = OtPpagePage;
    OtPpagePage.prototype.ionViewDidLoad = function () { };
    OtPpagePage.prototype.login2 = function () {
        this.navCtrl.setRoot("LoginPage");
    };
    OtPpagePage.prototype.getCodeBoxElement = function (index) {
        var inputValue = document.getElementById('codeBox' + index);
        return inputValue;
    };
    OtPpagePage.prototype.onKeyUpEvent = function (index, event) {
        var eventCode = event.which || event.keyCode;
        if (this.getCodeBoxElement(index).value.length === 1) {
            if (index !== 4) {
                this.getCodeBoxElement(index + 1).focus();
            }
            else {
                this.getCodeBoxElement(index).blur();
            }
        }
        if (eventCode === 8 && index !== 1) {
            this.getCodeBoxElement(index - 1).focus();
        }
    };
    OtPpagePage.prototype.onFocusEvent = function (index) {
        for (var item = 1; item < index; item++) {
            var currentElement = this.getCodeBoxElement(item);
            if (!currentElement.value) {
                currentElement.focus();
                break;
            }
        }
    };
    OtPpagePage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    OtPpagePage.prototype.doSignUpOtPS = function () {
        if (this.signinObj) {
            this.isUnchanged = true;
            var str = this.signinObj.split('@');
            var method = 'AuthenticateStlUserOTP';
            var parameters = [];
            this.username = str[0];
            this.otPpass = this.OTPForm.value.otp1.toString() + this.OTPForm.value.otp2.toString() + this.OTPForm.value.otp3.toString() + this.OTPForm.value.otp4.toString();
            this.validity = "30";
            var that_1 = this;
            this.url.startLoading().present();
            this.url.forExtraSecurity(function (err, resp) {
                if (resp) {
                    localStorage.setItem("newtm", resp);
                    var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                    var userNa = encr.getAddition1(that_1.username);
                    that_1.getSubCode(parameters, method, that_1, userNa);
                }
                else {
                    console.log("not getting response becz of err: ", err);
                    that_1.toastCtrl.create({
                        message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                        position: 'bottom',
                        duration: 3000
                    }).present();
                    that_1.url.stopLoading();
                    that_1.isUnchanged = false;
                }
            });
        }
    };
    OtPpagePage.userSignupgen = function (username, password) {
        var parameters = [];
        parameters["EMAIL_ID"] = username;
        parameters["PWD_LENGTH"] = password;
        return parameters;
    };
    OtPpagePage.userOTP = function (username, otPpass, validity) {
        var parameters = [];
        parameters["userId"] = username;
        parameters["OTP"] = otPpass;
        parameters["Validity"] = validity;
        return parameters;
    };
    OtPpagePage.prototype.getSubCode = function (parameters, method, that, userNa) {
        parameters['AuthenticateStlUserOTP   xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata"'] = OtPpagePage_1.userOTP(userNa, that.otPpass, that.validity);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // that.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                // that.url.stopLoading();
                var s = response['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].AuthenticateStlUserOTPResponse;
                var b = a[0].tuple[0].old[0].authenticateStlUserOTP[0].authenticateStlUserOTP[0];
                var signObj = that.signinObj;
                if (b == 'VALID') {
                    if (signObj) {
                        var str = signObj.split('@');
                        var aft = str[1];
                        var method = 'CreateSTLAuthUser';
                        var parameters = [];
                        that.url.forExtraSecurity(function (err, resp) {
                            if (resp) {
                                localStorage.setItem("newtm", resp);
                                var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                                var username = encr.getAddition1((signObj).toString().toLowerCase());
                                ;
                                var passwordLength = "6";
                                that.getSubCodeSubCode(parameters, method, that, username, passwordLength);
                            }
                            else {
                                console.log("not getting response becz of err: ", err);
                                that.toastCtrl.create({
                                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                                    position: 'bottom',
                                    duration: 3000
                                }).present();
                                that.url.stopLoading();
                            }
                        });
                    }
                    else {
                        var alert_1 = this.alertCtrl.create({
                            message: b,
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        that.OTPForm.reset();
                                    }
                                }]
                        });
                        alert_1.present();
                    }
                }
                else if (b == "INVALID/EXPIRED OTP") {
                    that.url.stopLoading();
                    that.isUnchanged = false;
                    var alert_2 = that.alertCtrl.create({
                        message: b,
                        buttons: [{
                                text: 'Okay',
                                handler: function () {
                                    that.OTPForm.reset();
                                }
                            }]
                    });
                    alert_2.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
                that.isUnchanged = false;
            }
        });
    };
    OtPpagePage.prototype.getSubCodeSubCode = function (parameters, method, that, username, passwordlength) {
        parameters['CreateSTLAuthUser xmlns="http://schemas.cordys.com/default"'] = OtPpagePage_1.userSignupgen(username, passwordlength);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // that.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.url.stopLoading();
                var toast = that.toastCtrl.create({
                    message: 'Your new password has been sent on your e-mail.',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.onDidDismiss(function () {
                    that.navCtrl.setRoot('LoginPage');
                });
                toast.present();
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    OtPpagePage = OtPpagePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-ot-ppage',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/ot-ppage/ot-ppage.html"*/'<ion-content class="bgClass1">\n  <ion-row>\n    <div class="logoDiv" style="margin-left: 22%">\n      <img src="assets/imgs/icon.png" />\n    </div>\n  </ion-row>\n\n  <ion-row class="bgClass">\n    <form [formGroup]="OTPForm" class="formcss">\n\n      <div class="otpBox">\n        <h5 style="margin-top: 10%;color:white; ">Enter One Time Password</h5>\n        <input id="codeBox1" type="number" maxlength="1" (keyup)="onKeyUpEvent(1, $event)" (onfocus)="onFocusEvent(1)"\n          formControlName="otp1" />\n        <input id="codeBox2" type="number" maxlength="1" (keyup)="onKeyUpEvent(2, $event)" (onfocus)="onFocusEvent(2)"\n          formControlName="otp2" />\n        <input id="codeBox3" type="number" maxlength="1" (keyup)="onKeyUpEvent(3, $event)" (onfocus)="onFocusEvent(3)"\n          formControlName="otp3" />\n        <input id="codeBox4" type="number" maxlength="1" (keyup)="onKeyUpEvent(4, $event)" (onfocus)="onFocusEvent(4)"\n          formControlName="otp4" />\n      </div>\n\n      <div style="text-align: center; padding-top: 25px;margin-left: 16%; ">\n        <button ion-button class="buttonClass1 " [disabled]="!OTPForm.valid || isUnchanged" (tap)="doSignUpOtPS()">Validate</button>\n      </div>\n      <div style="padding-top: 20px; text-align: center">\n        <div style="color: white; font-size: 15px; letter-spacing: 2px; display: block; float: left; right: 50%; position: relative; height: 100px;margin-top: 8%;">\n          Already Signed-Up?\n        </div>\n        <div class="buttonClass1" style="color: white; font-size: 15px;margin-top: 8%; letter-spacing: 2px;" (tap)="login2()">&nbsp;\n          LOGIN</div>\n      </div>\n    </form>\n  </ion-row>\n</ion-content>\n\n\n'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/ot-ppage/ot-ppage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__["a" /* HttpSoapProvider */]])
    ], OtPpagePage);
    return OtPpagePage;
    var OtPpagePage_1;
}());

//# sourceMappingURL=ot-ppage.js.map

/***/ })

});
//# sourceMappingURL=5.js.map