webpackJsonp([7],{

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPageModule", function() { return ChangePasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__change_password__ = __webpack_require__(361);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { SoapService } from '../../../providers/soap-service/soap-service';
var ChangePasswordPageModule = /** @class */ (function () {
    function ChangePasswordPageModule() {
    }
    ChangePasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__change_password__["a" /* ChangePasswordPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__change_password__["a" /* ChangePasswordPage */]),
            ],
            providers: [],
        })
    ], ChangePasswordPageModule);
    return ChangePasswordPageModule;
}());

//# sourceMappingURL=change-password.module.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TOKEN_KEY = 'session_token';
var ChangePasswordPage = /** @class */ (function () {
    function ChangePasswordPage(params, viewCtrl, fb, toastCtrl, url, navCtrl, storage, httpSoap) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.fb = fb;
        this.toastCtrl = toastCtrl;
        this.url = url;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.httpSoap = httpSoap;
        this.showForm = false;
        this.isUnchanged = false;
        this.userId = localStorage.getItem("userId");
        if (params.get('param') != null) {
            this.showForm = true;
            this.changePassForm = this.fb.group({
                oldPass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
                newPass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
                cnewPass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
            });
        }
        else {
            this.showForm = false;
            this.changePassForm = this.fb.group({
                oldPass: [localStorage.getItem("oldPass"), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
                newPass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
                cnewPass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
            });
        }
    }
    ChangePasswordPage_1 = ChangePasswordPage;
    ChangePasswordPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ChangePasswordPage.prototype.isOKfunction = function () {
        var method = 'IsOk';
        var parameters = [];
        var that = this;
        if (localStorage.getItem("StartInterval") != null) {
            this.url.forExtraSecurity(function (err, resp) {
                if (resp) {
                    localStorage.setItem("newtm", resp);
                    var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                    var encrUser = encr.getAddition1(localStorage.getItem("userId"));
                    that.userId = '';
                    var uid = encrUser;
                    that.storage.get(TOKEN_KEY).then(function (res) {
                        if (res) {
                            that.checkisOK(parameters, method, that, uid, res);
                        }
                    });
                }
            });
        }
    };
    ChangePasswordPage.isOkParams = function (userId, sessionid) {
        var parameters = [];
        parameters["User_Id"] = userId;
        parameters["session_id"] = sessionid;
        return parameters;
    };
    ChangePasswordPage.prototype.checkisOK = function (parameters, method, that, uid, sessionid) {
        parameters['IsOk xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = ChangePasswordPage_1.isOkParams(uid, sessionid);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                var trip = response;
                var s = trip['SOAP:Envelope'];
                if (s['SOAP:Body'][0]['SOAP:Fault']) {
                    that.callLogout();
                    var toast = that.toastCtrl.create({
                        message: "Session expired",
                        duration: 2000,
                        position: "top"
                    });
                    toast.present();
                }
            }
        });
    };
    ChangePasswordPage.prototype.submit = function () {
        var _this = this;
        this.isUnchanged = true;
        debugger;
        if (this.changePassForm.value.oldPass == this.changePassForm.value.newPass) {
            var toast = this.toastCtrl.create({
                message: 'Old password and New password can not be same!! Please try again..',
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                _this.changePassForm.patchValue({
                    newPass: '',
                    cnewPass: ''
                });
                _this.isUnchanged = false;
            });
            toast.present();
        }
        else {
            if (this.changePassForm.value.newPass != this.changePassForm.value.cnewPass) {
                var toast = this.toastCtrl.create({
                    message: 'New password and confirm password should matched!! Please try again..',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.onDidDismiss(function () {
                    _this.changePassForm.patchValue({
                        newPass: '',
                        cnewPass: ''
                    });
                    _this.isUnchanged = false;
                });
                toast.present();
            }
            else {
                var inID = JSON.parse(localStorage.getItem("intervalID"));
                clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("StartInterval");
                var method = 'ChangeSTLUserPassword';
                var parameters = [];
                var that_1 = this;
                this.url.startLoading().present();
                this.url.checkSession(function (err, resp) {
                    if (resp) {
                        localStorage.setItem("newtm", resp);
                        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                        var uid = encr.getAddition1(localStorage.getItem("userId"));
                        var oldpass = encr.getAddition2(that_1.changePassForm.value.oldPass);
                        var newPass = encr.getAddition2(that_1.changePassForm.value.newPass);
                        that_1.updatePassFunc(parameters, method, that_1, uid, oldpass, newPass);
                    }
                    else {
                        console.log("not getting response becz of err: ", err);
                        that_1.toastCtrl.create({
                            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                            position: 'bottom',
                            duration: 3000
                        }).present();
                        that_1.url.stopLoading();
                        that_1.isUnchanged = false;
                    }
                });
            }
        }
    };
    ChangePasswordPage.changePass = function (uid, opass, newpass) {
        var parameters = [];
        parameters["User_ID"] = uid;
        parameters["Old_Password"] = opass;
        parameters["New_Password"] = newpass;
        return parameters;
    };
    ChangePasswordPage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    ChangePasswordPage.prototype.updatePassFunc = function (parameters, method, that, uid, oldpass, newPass) {
        parameters['ChangeSTLUserPassword xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = ChangePasswordPage_1.changePass(uid, oldpass, newPass);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                var s = response['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].ChangeSTLUserPasswordResponse;
                var b = a[0].tuple[0].old[0].changeSTLUserPassword[0].changeSTLUserPassword[0];
                if (b == "Password changed sucessfully") {
                    var toast = that.toastCtrl.create({
                        message: 'You have sucessfully changed the password. Please login with your new password.',
                        duration: 5000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.callLogout();
                    });
                    toast.present();
                }
                else {
                    that.isUnchanged = false;
                    localStorage.setItem("StartInterval", "StartInterval");
                    if (localStorage.getItem("StartInterval") != null) {
                        that.session_id = setInterval(function () {
                            that.isOKfunction();
                        }, 30000);
                        localStorage.setItem("intervalID", that.session_id);
                    }
                    that.url.stopLoading();
                    var toast = that.toastCtrl.create({
                        message: 'Old password is wrong, Please try with valid password..',
                        duration: 2500,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.changePassForm.reset();
                    });
                    toast.present();
                }
            }
            else {
                that.isUnchanged = false;
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    ChangePasswordPage.prototype.callLogout = function () {
        var method = 'LogoutAuthUser';
        var parameters = [];
        var that = this;
        // this.url.checkSession(function (err, resp) {
        //     if (resp) {
        //         localStorage.setItem("newtm", resp)
        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
        var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        var uId = encrUser;
        that.logoutFunc(parameters, method, that, uId);
    };
    ChangePasswordPage.logoutParams = function (username) {
        var parameters = [];
        parameters["USER_ID"] = username;
        return parameters;
    };
    ChangePasswordPage.prototype.logoutFunc = function (parameters, method, that, uId) {
        parameters['LogoutAuthUser xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = ChangePasswordPage_1.logoutParams(uId);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        that.url.stopLoading();
        var inID = JSON.parse(localStorage.getItem("intervalID"));
        clearInterval(inID);
        that.navCtrl.setRoot("LoginPage");
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                // that.url.stopLoading();
                that.viewCtrl.dismiss();
                // var inID = JSON.parse(localStorage.getItem("intervalID"));
                // clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("LOGGEDIN");
                localStorage.removeItem("oldPass");
                localStorage.removeItem("userId");
                localStorage.removeItem("userName");
                localStorage.removeItem("userdetail");
                localStorage.removeItem("trewq");
                localStorage.removeItem("gfdsa");
                localStorage.removeItem("newtm");
                localStorage.removeItem("StartInterval");
                that.storage.remove("session_token");
                // that.navCtrl.setRoot("LoginPage");
            }
            else {
                // console.log("not getting response becz of err: ", err)
                // that.toastCtrl.create({
                //     message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                //     position: 'bottom',
                //     duration: 3000
                // }).present();
                // that.url.stopLoading();
                that.viewCtrl.dismiss();
                // var inID = JSON.parse(localStorage.getItem("intervalID"));
                // clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("LOGGEDIN");
                localStorage.removeItem("oldPass");
                localStorage.removeItem("userId");
                localStorage.removeItem("userName");
                localStorage.removeItem("userdetail");
                localStorage.removeItem("trewq");
                localStorage.removeItem("gfdsa");
                localStorage.removeItem("newtm");
                localStorage.removeItem("StartInterval");
                that.storage.remove("session_token");
            }
        });
    };
    ChangePasswordPage = ChangePasswordPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-change-password',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/home/change-password/change-password.html"*/'<ion-content padding class="guide-modal">\n\n\n\n    <ion-icon *ngIf="showForm" class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n    <div>\n\n        <p style="text-align: center; font-size: 2rem">\n\n            <b>Change Password</b>\n\n        </p>\n\n        <form *ngIf="!showForm" [formGroup]="changePassForm">\n\n            <ion-row [hidden]="true">\n\n                <ion-col col-6>Old Password: </ion-col>\n\n                <ion-col col-6>\n\n                    <ion-input type="password" formControlName="oldPass" readonly></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6>New Password:</ion-col>\n\n                <ion-col col-6>\n\n                    <ion-input type="password" formControlName="newPass"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6>Confirm Password:</ion-col>\n\n                <ion-col col-6>\n\n                    <ion-input type="password" formControlName="cnewPass"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-12>\n\n                    <button ion-button block color="sterlite" [disabled]="!changePassForm.valid || isUnchanged" (tap)="submit()">Submit</button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </form>\n\n\n\n        <form *ngIf="showForm" [formGroup]="changePassForm">\n\n            <ion-row>\n\n                <ion-col col-6>Old Password: </ion-col>\n\n                <ion-col col-6>\n\n                    <ion-input type="password" formControlName="oldPass"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6>New Password:</ion-col>\n\n                <ion-col col-6>\n\n                    <ion-input type="password" formControlName="newPass"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6>Confirm Password:</ion-col>\n\n                <ion-col col-6>\n\n                    <ion-input type="password" formControlName="cnewPass"></ion-input>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-12>\n\n                    <button ion-button block color="sterlite" [disabled]="!changePassForm.valid || isUnchanged" (tap)="submit()">Submit</button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </form>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/home/change-password/change-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_6__providers_http_soap_http_soap__["a" /* HttpSoapProvider */]])
    ], ChangePasswordPage);
    return ChangePasswordPage;
    var ChangePasswordPage_1;
}());

//# sourceMappingURL=change-password.js.map

/***/ })

});
//# sourceMappingURL=7.js.map