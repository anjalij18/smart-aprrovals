webpackJsonp([8],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return URLS; });
var URLS = /** @class */ (function () {
    function URLS() {
        this._baseURL = "https://1dottest.sterliteapps.com/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=Sterlite,cn=cordys,cn=UATSterlite,o=STLAD.COM";
        // _baseURL: string = "http://115.113.138.76/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=travelportal,cn=cordys,cn=STLDevNew,o=STLAD.COM";
        // _baseURL: string = "https://1dot.sterliteapps.com/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=Sterlite,cn=cordys,cn=SterlitePrd,o=STLAD.COM";
    }
    return URLS;
}());

//# sourceMappingURL=urls.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__http_soap_http_soap__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__urls__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TOKEN_KEY = 'session_token';
var HttpServiceProvider = /** @class */ (function () {
    function HttpServiceProvider(http, loadingCtrl, storage, httpSoap, toastCtrl, urls) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.httpSoap = httpSoap;
        this.toastCtrl = toastCtrl;
        this.urls = urls;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.servicePort = '';
        console.log('Hello HttpServiceProvider Provider');
        this.servicePort = this.servicePort;
        this.servicePath = this.urls._baseURL;
    }
    HttpServiceProvider_1 = HttpServiceProvider;
    HttpServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    HttpServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    HttpServiceProvider.prototype.startLoadingToast = function () {
        return this.loadingToast = this.toastCtrl.create({
            message: "Please wait while the content is loading...",
            position: "bottom"
        });
    };
    HttpServiceProvider.prototype.stopLoadingToast = function () {
        return this.loadingToast.dismiss();
    };
    HttpServiceProvider.prototype.servicePathUrl = function () {
        return this.servicePath;
    };
    HttpServiceProvider.prototype.servicePortUrl = function () {
        return this.servicePort;
    };
    HttpServiceProvider.prototype.approvepoCall = function (taskid) {
        return this.http.get(this.servicePort + 'http://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve', { headers: this.headers })
            .map(function (res) { return res; });
    };
    HttpServiceProvider.prototype.rejectCall = function (taskid) {
        // return this.http.get(this.servicePort + 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject', { headers: this.headers })
        return this.http.get(this.servicePort + 'http://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject', { headers: this.headers })
            .map(function (res) { return res; });
    };
    HttpServiceProvider.prototype.checkSession = function (cb) {
        var parameters = [];
        parameters['GetCurrentTimeStamp xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HttpServiceProvider_1.userSessionPara();
        parameters = [{ "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } }];
        var that = this;
        this.httpSoap.testService(parameters[0], function (err, response) {
            if (response) {
                var trip = response;
                var s = trip['SOAP:Envelope'];
                try {
                    var a = s['SOAP:Body'][0].GetCurrentTimeStampResponse;
                    var b = a[0].tuple[0].old[0].getCurrentTimeStamp[0].getCurrentTimeStamp[0];
                    that.timestamp = b;
                    localStorage.setItem("trewq", that.timestamp);
                    that.storage.set("trewq", that.timestamp);
                    that.storage.get(TOKEN_KEY).then(function (res) {
                        if (res) {
                            that.userId = localStorage.getItem("userId");
                            var a = new __WEBPACK_IMPORTED_MODULE_5_jsencrypt__["JSEncrypt"](); // called third party js library for secury purpose
                            var strstr = that.userId + "$$$" + res;
                            a.getAddition(strstr);
                            cb(err, that.timestamp);
                        }
                    });
                }
                catch (e) {
                    if (that.loading) {
                        that.loading.dismiss();
                    }
                    if (that.loadingToast) {
                        that.loadingToast.dismiss();
                    }
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    var toast = that.toastCtrl.create({
                        message: "Something went wrong. Please try again in some time.",
                        duration: 3000,
                        position: "top"
                    });
                    toast.present();
                }
            }
            else {
                cb(err, that.timestamp);
            }
        });
    };
    HttpServiceProvider.prototype.forExtraSecurity = function (cb) {
        var parameters = [];
        parameters['GetCurrentTimeStamp xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HttpServiceProvider_1.userSessionPara();
        parameters = [{ "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } }];
        var that = this;
        // debugger
        this.httpSoap.testService(parameters[0], function (err, response) {
            if (response) {
                var trip = response;
                var s = trip['SOAP:Envelope'];
                try {
                    var a = s['SOAP:Body'][0].GetCurrentTimeStampResponse;
                    var b = a[0].tuple[0].old[0].getCurrentTimeStamp[0].getCurrentTimeStamp[0];
                    that.timestamp = b;
                    cb(err, that.timestamp);
                }
                catch (e) {
                    if (that.loading) {
                        that.loading.dismiss();
                    }
                    if (that.loadingToast) {
                        that.loadingToast.dismiss();
                    }
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    // console.log("catch token expired: ", b)
                    var toast = that.toastCtrl.create({
                        message: "Something went wrong. Please try again in some time.",
                        duration: 3000,
                        position: "top"
                    });
                    toast.present();
                }
            }
            else {
                cb(err, response);
            }
        });
    };
    HttpServiceProvider.userSessionPara = function () {
        var parameters;
        parameters = "";
        return parameters;
    };
    HttpServiceProvider = HttpServiceProvider_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_6__http_soap_http_soap__["a" /* HttpSoapProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_7__urls__["a" /* URLS */]])
    ], HttpServiceProvider);
    return HttpServiceProvider;
    var HttpServiceProvider_1;
}());

//# sourceMappingURL=http-service.js.map

/***/ }),

/***/ 145:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 145;

/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/home/change-password/change-password.module": [
		349,
		7
	],
	"../pages/home/home.module": [
		350,
		6
	],
	"../pages/login/login.module": [
		351,
		1
	],
	"../pages/ot-ppage/ot-ppage.module": [
		352,
		5
	],
	"../pages/po-list/po-details/po-details.module": [
		353,
		4
	],
	"../pages/po-list/po-list.module": [
		354,
		3
	],
	"../pages/pr-list/pr-details/pr-details.module": [
		355,
		2
	],
	"../pages/pr-list/pr-list.module": [
		356,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 186;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverPO; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_opener__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_document_viewer__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_photo_viewer__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var PopoverPO = /** @class */ (function () {
    function PopoverPO(httpSer, photoViewer, document, alertCtrl, app, platform, navP, file, fileOpener, viewCtrl, toastCtrl, httpSoap, navCtrl) {
        var _this = this;
        this.httpSer = httpSer;
        this.photoViewer = photoViewer;
        this.document = document;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.platform = platform;
        this.navP = navP;
        this.file = file;
        this.fileOpener = fileOpener;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.httpSoap = httpSoap;
        this.navCtrl = navCtrl;
        this.udata = [];
        console.log("navParams=> ", navP.get("poData"));
        var dData = navP.get("poData");
        this.udata = dData[0].item;
        console.log("udata data: => ", this.udata);
        this.POnumber = navP.get("poNumber");
        this.platform.registerBackButtonAction(function () {
            // Catches the active view
            var nav = _this.app.getActiveNavs()[0];
            var activeView = nav.getActive();
            if (activeView.name === "HomePage") {
                var alert_1 = _this.alertCtrl.create({
                    title: 'App termination',
                    message: 'Do you want to close the app?',
                    buttons: [{
                            text: 'Cancel',
                            role: 'cancel',
                            handler: function () {
                                console.log('Application exit prevented!');
                            }
                        }, {
                            text: 'Close App',
                            handler: function () {
                                _this.platform.exitApp(); // Close this application
                            }
                        }]
                });
                alert_1.present();
                // }
            }
            else {
                if (activeView.name === 'PoDetailsPage') {
                    _this.viewCtrl.dismiss();
                    nav.pop();
                }
                else {
                    if (nav.canGoBack()) {
                        nav.pop();
                    }
                }
            }
        });
    }
    PopoverPO_1 = PopoverPO;
    PopoverPO.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.viewCtrl.dismiss();
        }, 10000);
    };
    PopoverPO.prototype.getDocsListContent = function (doc) {
        var that = this;
        this.viewCtrl.dismiss();
        // that.tk = undefined;
        // this.httpSer.checkSession(function (err, resp) {
        //   if (resp) {
        // that.tk = localStorage.getItem("gfdsa");
        // localStorage.setItem("newtm", resp);
        that.getDocsListContentFunc(doc);
        //   } else {
        //     console.log("not getting response becz of err: ", err)
        //     that.toastCtrl.create({
        //       message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
        //       position: 'bottom',
        //       duration: 3000
        //     }).present();
        //   }
        // });
    };
    PopoverPO.prototype.getDocsListContentFunc = function (doc) {
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_7_jsencrypt__["JSEncrypt"]();
        // var enrPOnumber = encr.getAddition1(that.POnumber[0]);
        // var enrAtchId = encr.getAddition1(doc.old[0].STL_PO_ATTACHMENTS[0].ATTACH_ID[0]);
        // var encryptedPOnum = enrPOnumber;
        // var encryptedAtchID = enrAtchId;
        // var enrPOnumber = encr.getAddition1(that.POnumber[0]);
        // var enrAtchId = encr.getAddition1(doc.DOCID[0]);
        // var encryptedPOnum = enrPOnumber;
        // var encryptedAtchID = enrAtchId;
        var encryptedPOnum = that.POnumber[0];
        var encryptedAtchID = doc.DOCID[0];
        that.getBase64Format(encryptedPOnum, parameters, encryptedAtchID, doc);
    };
    PopoverPO.prototype.getBase64Format = function (encryptedPOnum, parameters, enrAtchId, doc) {
        var that = this;
        // parameters['GetPOAttachmentContent xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PopoverPO.docsPODetails(encryptedPOnum, enrAtchId, that.tk);
        // parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        parameters['GetPOAttachmentContentFromSAP xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PopoverPO_1.docsPODetails(encryptedPOnum, enrAtchId, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        that.httpSer.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            that.httpSer.stopLoading();
            if (response) {
                var temp = response;
                var s = temp['SOAP:Envelope'];
                try {
                    // var a = s['SOAP:Body'][0].GetPOAttachmentContentResponse;
                    // var b64 = a[0].return;
                    // that.b64Data = b64[0];
                    // console.log("base64 code: ", that.b64Data)
                    // that.download(that.b64Data, doc)
                    var a = s['SOAP:Body'][0].GetPOAttachmentContentFromSAPResponse;
                    var b64 = a[0].ZMM_PO_READ_FILE_ATTACHMENTResponse;
                    that.b64Data = b64[0].EV_FILE[0]._;
                    console.log("base64 code: ", that.b64Data);
                    that.download(that.b64Data, doc);
                }
                catch (e) {
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    console.log("token err: ", JSON.stringify(b));
                    var toast = that.toastCtrl.create({
                        message: "Token expired!! Please refresh once..",
                        duration: 2500,
                        position: 'top'
                    });
                    toast.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
            }
        });
    };
    PopoverPO.docsPODetails = function (po_num, atch_id, tk) {
        var parameters = [];
        parameters["poNumber"] = po_num;
        parameters["attachId"] = atch_id;
        // parameters["tk"] = tk;
        return parameters;
    };
    PopoverPO.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    PopoverPO.prototype.getMIMEtype = function (extn) {
        var ext = extn.toLowerCase();
        var MIMETypes = {
            'msg': 'text/plain',
            'txt': 'text/plain',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'pdf': 'application/pdf',
            'jpg': 'image/jpeg',
            'bmp': 'image/bmp',
            'png': 'image/png',
            'xls': 'application/vnd.ms-excel',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'rtf': 'application/rtf',
            'ppt': 'application/vnd.ms-powerpoint',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'zip': 'application/zip'
        };
        return MIMETypes[ext];
    };
    PopoverPO.prototype.download = function (b64, doc) {
        var _this = this;
        var downloadPDF = b64;
        fetch('data:application/pdf;base64,' + downloadPDF, {
            method: "GET"
        }).then(function (res) { return res.blob(); }).then(function (blob) {
            var fileExtn = doc.FILENAME[0].split('.').reverse()[0];
            console.log("file extention: ", fileExtn);
            var fileMIMEType = _this.getMIMEtype(fileExtn);
            if (_this.platform.is('android')) {
                // this.file.writeFile(this.file.externalApplicationStorageDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {
                _this.file.writeFile(_this.file.cacheDirectory, doc.FILENAME[0], blob, { replace: true }).then(function (res) {
                    var docSrc1 = res.toInternalURL();
                    docSrc1 = decodeURIComponent(docSrc1);
                    console.log("doc string: ", docSrc1);
                    // window.open(docSrc1, '_blank');
                    _this.fileOpener.open(docSrc1, fileMIMEType).then(function (res) {
                        console.log("file opend: ", res);
                    }).catch(function (err) {
                        console.log('open error', err);
                    });
                }).catch(function (err) {
                    console.log('save error');
                });
            }
            else if (_this.platform.is('ios')) {
                // this.file.writeFile(this.file.documentsDirectory, doc.FILENAME[0], blob, { replace: true }).then(res => {
                _this.file.writeFile(_this.file.cacheDirectory, doc.FILENAME[0], blob, { replace: true }).then(function (res) {
                    if (fileMIMEType == 'application/pdf') {
                        var options = {
                            title: doc.FILENAME[0]
                        };
                        _this.document.viewDocument(res.nativeURL, fileMIMEType, options);
                    }
                    else {
                        if (fileMIMEType == 'image/jpeg' || fileMIMEType == 'image/png' || fileMIMEType == 'image/jpg') {
                            console.log("nativeURL: ", res.nativeURL);
                            var imgSrc = res.nativeURL;
                            imgSrc = decodeURIComponent(imgSrc);
                            console.log("decodeURIComponent: ", imgSrc);
                            _this.photoViewer.show(imgSrc);
                        }
                        else {
                            var docSrc = res.toInternalURL();
                            docSrc = decodeURIComponent(docSrc);
                            console.log("doc src: ", docSrc);
                            _this.fileOpener.open(docSrc, fileMIMEType).then(function (res) {
                                console.log("file opend: ", res);
                            }).catch(function (err) {
                                console.log('open error', err);
                            });
                        }
                    }
                }).catch(function (err) {
                    console.log('save error');
                });
            }
        }).catch(function (err) {
            console.log('error');
        });
    };
    PopoverPO = PopoverPO_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: "\n  <ion-list class=\"maindiv\">\n    <ion-item *ngFor=\"let doc of udata\" (click)=\"getDocsListContent(doc)\" style=\"background-color: transparent;\">\n      <p ion-text text-wrap  style=\"background-color: transparent; color:white;\">{{doc.FILENAME[0]}}</p>\n    </ion-item>\n  </ion-list>\n\n    ",
            styles: ["\n\n    .maindiv{\n      background-color: #0065b3;\n    }\n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_document_viewer__["a" /* DocumentViewer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_opener__["a" /* FileOpener */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_http_soap_http_soap__["a" /* HttpSoapProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]])
    ], PopoverPO);
    return PopoverPO;
    var PopoverPO_1;
}());

// @Component({
//   template: `
// <ion-content>
// <pdf-viewer *ngIf="pdfSrc != undefined" [src]="pdfSrc"
// [render-text]="true"
// style="display: block;"
// ></pdf-viewer></ion-content>`
// })
// export class DocViewPage {
//   pdfSrc: any;
//   constructor(navParams: NavParams) {
//     console.log("new page nav params: "+ navParams.get("param"));
//     this.pdfSrc = navParams.get("param");
//   }
// }
//# sourceMappingURL=popover-po.js.map

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverPR; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopoverPR = /** @class */ (function () {
    function PopoverPR(navP) {
        this.navP = navP;
        this.udata = [];
        console.log("navParams=> ", navP.get("prData"));
        this.udata = navP.get("prData");
    }
    PopoverPR = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: "\n    <table>\n        <tr>\n            <td>RELEASE CODE:</td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_RELEASECODE}}</td>\n        </tr>\n            <tr>\n            <td>RELEASE GROUP:</td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_RELEASEGROUP}}</td>\n        </tr>\n        <tr>\n            <td>Line Number:</td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER}}</td>\n        </tr>\n        <tr>\n            <td>Material Desc: </td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_MATERIAL_DESC}}</td>\n        </tr>\n        <tr>\n            <td>Qunatity: </td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_QUANTITY}}</td>\n        </tr>\n        <tr>\n            <td>Net Value: </td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].PRR_NET_VALUE}}</td>\n        </tr>\n        <tr>\n            <td>GL Account:</td>\n            <td>{{udata.old[0].PurchaseRequisitionRelease[0].GL_ACCOUNT}}</td>\n        </tr>\n    </table>\n    ",
            styles: ["\n     table {   \n        table-layout: fixed;\n        width: 100%;\n        border-collapse: collapse;\n        border: 1px solid black;\n       }\n      \n      thead th:nth-child(1) {\n        width: 30%;\n      }\n      \n      thead th:nth-child(2) {\n        width: 20%;\n      }\n      \n      thead th:nth-child(3) {\n        width: 15%;\n      }\n      \n      thead th:nth-child(4) {\n        width: 35%;\n      }\n      \n      th, td {\n        padding: 8px;\n      }\n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], PopoverPR);
    return PopoverPR;
}());

//# sourceMappingURL=popover-pr.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(269);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorHandler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_pro__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_pro___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__ionic_pro__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_po_list_po_details_popover_po_popover_po__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_pr_list_pr_details_popover_pr_popover_pr__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_http_soap_http_soap__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_http__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_urls__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_file__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_file_opener__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_document_viewer__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_photo_viewer__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





















// import { PdfViewerModule } from 'ng2-pdf-viewer';
__WEBPACK_IMPORTED_MODULE_5__ionic_pro__["Pro"].init('e3a06060', {
    appVersion: '0.0.1'
});
var MyErrorHandler = /** @class */ (function () {
    function MyErrorHandler(injector) {
        try {
            this.ionicErrorHandler = injector.get(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */]);
        }
        catch (e) {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }
    MyErrorHandler.prototype.handleError = function (err) {
        __WEBPACK_IMPORTED_MODULE_5__ionic_pro__["Pro"].monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    };
    MyErrorHandler = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Injector */]])
    ], MyErrorHandler);
    return MyErrorHandler;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_po_list_po_details_popover_po_popover_po__["a" /* PopoverPO */],
                __WEBPACK_IMPORTED_MODULE_10__pages_pr_list_pr_details_popover_pr_popover_pr__["a" /* PopoverPR */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/home/change-password/change-password.module#ChangePasswordPageModule', name: 'ChangePasswordPage', segment: 'change-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ot-ppage/ot-ppage.module#OtPpagePageModule', name: 'OtPpagePage', segment: 'ot-ppage', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/po-list/po-details/po-details.module#PoDetailsPageModule', name: 'PoDetailsPage', segment: 'po-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/po-list/po-list.module#PoListPageModule', name: 'PoListPage', segment: 'po-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pr-list/pr-details/pr-details.module#PrDetailsPageModule', name: 'PrDetailsPage', segment: 'pr-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pr-list/pr-list.module#PRListPageModule', name: 'PRListPage', segment: 'pr-list', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_po_list_po_details_popover_po_popover_po__["a" /* PopoverPO */],
                __WEBPACK_IMPORTED_MODULE_10__pages_pr_list_pr_details_popover_pr_popover_pr__["a" /* PopoverPR */],
            ],
            providers: [
                // { provide: String, useValue: "SoapService" },
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */],
                [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: MyErrorHandler }],
                __WEBPACK_IMPORTED_MODULE_8__providers_http_service_http_service__["a" /* HttpServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_12__providers_http_soap_http_soap__["a" /* HttpSoapProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_urls__["a" /* URLS */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_document_viewer__["a" /* DocumentViewer */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 312:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 314:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(244);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, app, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.app = app;
        this.alertCtrl = alertCtrl;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.initializeAPP();
        platform.registerBackButtonAction(function () {
            var nav = app.getActiveNavs()[0];
            var activeView = nav.getActive();
            if (activeView.name === "HomePage") {
                var alert_1 = _this.alertCtrl.create({
                    title: 'App termination',
                    message: 'Do you want to close the app?',
                    buttons: [{
                            text: 'Cancel',
                            role: 'cancel',
                            handler: function () {
                                console.log('Application exit prevented!');
                            }
                        }, {
                            text: 'Close App',
                            handler: function () {
                                _this.platform.exitApp(); // Close this application
                            }
                        }]
                });
                alert_1.present();
            }
            else {
                if (nav.canGoBack()) {
                    nav.pop();
                }
            }
        });
    }
    MyApp.prototype.initializeAPP = function () {
        if (localStorage.getItem('LOGGEDIN') != null) {
            this.rootPage = "HomePage";
        }
        else {
            this.rootPage = "LoginPage";
            console.log("Login page");
        }
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpSoapProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xml2js__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xml2js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_xml2js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__urls__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HttpSoapProvider = /** @class */ (function () {
    function HttpSoapProvider(urls, http, httpnew) {
        this.urls = urls;
        this.http = http;
        this.httpnew = httpnew;
        this.envelopeBuilder_ = null;
        console.log('Hello HttpSoapProvider Provider');
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'text/xml;charset="utf-8"' });
    }
    HttpSoapProvider_1 = HttpSoapProvider;
    HttpSoapProvider.prototype.toXml = function (parameters) {
        var xml = "";
        var parameter;
        switch (typeof (parameters)) {
            case "string":
                xml += parameters.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                break;
            case "number":
            case "boolean":
                xml += parameters.toString();
                break;
            case "object":
                if (parameters.constructor.toString().indexOf("function Date()") > -1) {
                    var year = parameters.getFullYear().toString();
                    var month = ("0" + (parameters.getMonth() + 1).toString()).slice(-2);
                    var date = ("0" + parameters.getDate().toString()).slice(-2);
                    var hours = ("0" + parameters.getHours().toString()).slice(-2);
                    var minutes = ("0" + parameters.getMinutes().toString()).slice(-2);
                    var seconds = ("0" + parameters.getSeconds().toString()).slice(-2);
                    var milliseconds = parameters.getMilliseconds().toString();
                    var tzOffsetMinutes = Math.abs(parameters.getTimezoneOffset());
                    var tzOffsetHours = 0;
                    while (tzOffsetMinutes >= 60) {
                        tzOffsetHours++;
                        tzOffsetMinutes -= 60;
                    }
                    var tzMinutes = ("0" + tzOffsetMinutes.toString()).slice(-2);
                    var tzHours = ("0" + tzOffsetHours.toString()).slice(-2);
                    var timezone = ((parameters.getTimezoneOffset() < 0) ? "-" : "+") + tzHours + ":" + tzMinutes;
                    xml += year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds + "." + milliseconds + timezone;
                }
                else if (parameters.constructor.toString().indexOf("function Array()") > -1) {
                    for (parameter in parameters) {
                        if (parameters.hasOwnProperty(parameter)) {
                            if (!isNaN(parameter)) {
                                (/function\s+(\w*)\s*\(/ig).exec(parameters[parameter].constructor.toString());
                                var type = RegExp.$1;
                                switch (type) {
                                    case "":
                                        type = typeof (parameters[parameter]);
                                        break;
                                    case "String":
                                        type = "string";
                                        break;
                                    case "Number":
                                        type = "int";
                                        break;
                                    case "Boolean":
                                        type = "bool";
                                        break;
                                    case "Date":
                                        type = "DateTime";
                                        break;
                                }
                                xml += this.toElement(type, parameters[parameter]);
                            }
                            else {
                                xml += this.toElement(parameter, parameters[parameter]);
                            }
                        }
                    }
                }
                else {
                    for (parameter in parameters) {
                        if (parameters.hasOwnProperty(parameter)) {
                            xml += this.toElement(parameter, parameters[parameter]);
                        }
                    }
                }
                break;
            default:
                throw new Error("SoapService error: type '" + typeof (parameters) + "' is not supported");
        }
        return xml;
    };
    HttpSoapProvider.stripTagAttributes = function (tagNamePotentiallyWithAttributes) {
        tagNamePotentiallyWithAttributes = tagNamePotentiallyWithAttributes + ' ';
        return tagNamePotentiallyWithAttributes.slice(0, tagNamePotentiallyWithAttributes.indexOf(' '));
    };
    HttpSoapProvider.prototype.toElement = function (tagNamePotentiallyWithAttributes, parameters) {
        var elementContent = this.toXml(parameters);
        if ("" == elementContent) {
            return "<" + tagNamePotentiallyWithAttributes + "/>";
        }
        else {
            return "<" + tagNamePotentiallyWithAttributes + ">" + elementContent + "</" + HttpSoapProvider_1.stripTagAttributes(tagNamePotentiallyWithAttributes) + ">";
        }
    };
    Object.defineProperty(HttpSoapProvider.prototype, "envelopeBuilder", {
        set: function (envelopeBuilder) {
            this.envelopeBuilder_ = envelopeBuilder;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpSoapProvider.prototype, "envelopeBuilder2", {
        set: function (envelopeBuilder2) {
            this.envelopeBuilder_ = envelopeBuilder2;
        },
        enumerable: true,
        configurable: true
    });
    HttpSoapProvider.prototype.testService = function (input, cb) {
        var _this = this;
        var request = this.toXml(input);
        var envelopedRequest = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;
        this.http.setSSLCertMode('pinned').then(function (res) {
            var headers = {
                'Content-Type': 'text/xml;charset="utf-8"',
                "Accept": "text/xml",
                'origin': 'Mobile'
            };
            var soapurl = _this.urls._baseURL;
            var xml = envelopedRequest;
            _this.http.setDataSerializer('utf8');
            _this.http.post(soapurl, xml, headers)
                .then(function (data) {
                debugger;
                __WEBPACK_IMPORTED_MODULE_2_xml2js__["parseString"](data.data, function (err, result) {
                    if (result == undefined) {
                        cb(err, result);
                    }
                    else {
                        cb(err, result);
                    }
                });
            }).catch(function (err) {
                debugger;
                __WEBPACK_IMPORTED_MODULE_2_xml2js__["parseString"](err.error, function (err, result) {
                    if (result == undefined) {
                        cb(err, result);
                    }
                    else {
                        cb(err, result);
                    }
                });
            });
        });
    };
    HttpSoapProvider = HttpSoapProvider_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__urls__["a" /* URLS */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], HttpSoapProvider);
    return HttpSoapProvider;
    var HttpSoapProvider_1;
}());

//# sourceMappingURL=http-soap.js.map

/***/ })

},[248]);
//# sourceMappingURL=main.js.map