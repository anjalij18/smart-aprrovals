webpackJsonp([0],{

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRListPageModule", function() { return PRListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pr_list__ = __webpack_require__(368);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PRListPageModule = /** @class */ (function () {
    function PRListPageModule() {
    }
    PRListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__pr_list__["a" /* PRListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__pr_list__["a" /* PRListPage */]),
                __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["b" /* IonPullupModule */]
            ],
        })
    ], PRListPageModule);
    return PRListPageModule;
}());

//# sourceMappingURL=pr-list.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return IonPullUpFooterState; });
/* unused harmony export IonPullUpFooterBehavior */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonPullUpComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/*
ionic-pullup v2 for Ionic/Angular 2
 
Copyright 2016 Ariel Faur (https://github.com/arielfaur)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


var IonPullUpFooterState;
(function (IonPullUpFooterState) {
    IonPullUpFooterState[IonPullUpFooterState["Collapsed"] = 0] = "Collapsed";
    IonPullUpFooterState[IonPullUpFooterState["Expanded"] = 1] = "Expanded";
    IonPullUpFooterState[IonPullUpFooterState["Minimized"] = 2] = "Minimized";
})(IonPullUpFooterState || (IonPullUpFooterState = {}));
var IonPullUpFooterBehavior;
(function (IonPullUpFooterBehavior) {
    IonPullUpFooterBehavior[IonPullUpFooterBehavior["Hide"] = 0] = "Hide";
    IonPullUpFooterBehavior[IonPullUpFooterBehavior["Expand"] = 1] = "Expand";
})(IonPullUpFooterBehavior || (IonPullUpFooterBehavior = {}));
var IonPullUpComponent = (function () {
    function IonPullUpComponent(platform, el, renderer) {
        this.platform = platform;
        this.el = el;
        this.renderer = renderer;
        this.stateChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onExpand = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onCollapse = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onMinimize = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._footerMeta = {
            height: 0,
            posY: 0,
            lastPosY: 0
        };
        this._currentViewMeta = {};
        // sets initial state
        this.initialState = this.initialState || IonPullUpFooterState.Collapsed;
        this.defaultBehavior = this.defaultBehavior || IonPullUpFooterBehavior.Expand;
        this.maxHeight = this.maxHeight || 0;
    }
    IonPullUpComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.debug('ionic-pullup => Initializing footer...');
        window.addEventListener("orientationchange", function () {
            console.debug('ionic-pullup => Changed orientation => updating');
            _this.updateUI();
        });
        this.platform.resume.subscribe(function () {
            console.debug('ionic-pullup => Resumed from background => updating');
            _this.updateUI();
        });
    };
    IonPullUpComponent.prototype.ngAfterContentInit = function () {
        this.computeDefaults();
        this.state = IonPullUpFooterState.Collapsed;
        this.updateUI(true); // need to indicate whether it's first run to avoid emitting events twice due to change detection
    };
    Object.defineProperty(IonPullUpComponent.prototype, "expandedHeight", {
        get: function () {
            return window.innerHeight - this._currentViewMeta.headerHeight;
        },
        enumerable: true,
        configurable: true
    });
    IonPullUpComponent.prototype.computeDefaults = function () {
        this._footerMeta.defaultHeight = this.childFooter.nativeElement.offsetHeight;
        // TODO: still need to test with tabs template (not convinced it is a valid use case...)
        this._currentViewMeta.tabs = this.el.nativeElement.closest('ion-tabs');
        this._currentViewMeta.tabsHeight = this._currentViewMeta.tabs ? this._currentViewMeta.tabs.querySelector('.tabbar').offsetHeight : 0;
        console.debug(this._currentViewMeta.tabsHeight ? 'ionic-pullup => Tabs detected' : 'ionic.pullup => View has no tabs');
        //this._currentViewMeta.hasBottomTabs = this._currentViewMeta.tabs && this._currentViewMeta.tabs.classList.contains('tabs-bottom');
        this._currentViewMeta.header = document.querySelector('ion-navbar.toolbar');
        this._currentViewMeta.headerHeight = this._currentViewMeta.header ? this._currentViewMeta.header.offsetHeight : 0;
    };
    IonPullUpComponent.prototype.computeHeights = function (isInit) {
        if (isInit === void 0) { isInit = false; }
        this._footerMeta.height = this.maxHeight > 0 ? this.maxHeight : this.expandedHeight;
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'height', this._footerMeta.height + 'px');
        // TODO: implement minimize mode
        //this.renderer.setElementStyle(this.el.nativeElement, 'min-height', this._footerMeta.height + 'px'); 
        //if (this.initialState == IonPullUpFooterState.Minimized) {
        //  this.minimize()  
        //} else {
        this.collapse(isInit);
        //} 
    };
    IonPullUpComponent.prototype.updateUI = function (isInit) {
        var _this = this;
        if (isInit === void 0) { isInit = false; }
        setTimeout(function () {
            _this.computeHeights(isInit);
        }, 300);
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', 'none'); // avoids flickering when changing orientation
    };
    IonPullUpComponent.prototype.expand = function () {
        this._footerMeta.lastPosY = 0;
        this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, 0, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, 0, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', '300ms ease-in-out');
        this.onExpand.emit(null);
    };
    IonPullUpComponent.prototype.collapse = function (isInit) {
        if (isInit === void 0) { isInit = false; }
        this._footerMeta.lastPosY = this._footerMeta.height - this._footerMeta.defaultHeight - this._currentViewMeta.tabsHeight;
        this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        if (!isInit)
            this.onCollapse.emit(null);
    };
    IonPullUpComponent.prototype.minimize = function () {
        this._footerMeta.lastPosY = this._footerMeta.height;
        this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        this.onMinimize.emit(null);
    };
    IonPullUpComponent.prototype.onTap = function (e) {
        e.preventDefault();
        if (this.state == IonPullUpFooterState.Collapsed) {
            if (this.defaultBehavior == IonPullUpFooterBehavior.Hide)
                this.state = IonPullUpFooterState.Minimized;
            else
                this.state = IonPullUpFooterState.Expanded;
        }
        else {
            if (this.state == IonPullUpFooterState.Minimized) {
                if (this.defaultBehavior == IonPullUpFooterBehavior.Hide)
                    this.state = IonPullUpFooterState.Collapsed;
                else
                    this.state = IonPullUpFooterState.Expanded;
            }
            else {
                // footer is expanded
                this.state = this.initialState == IonPullUpFooterState.Minimized ? IonPullUpFooterState.Minimized : IonPullUpFooterState.Collapsed;
            }
        }
    };
    IonPullUpComponent.prototype.onDrag = function (e) {
        e.preventDefault();
        switch (e.type) {
            case 'panstart':
                this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', 'none');
                break;
            case 'pan':
                this._footerMeta.posY = Math.round(e.deltaY) + this._footerMeta.lastPosY;
                if (this._footerMeta.posY < 0 || this._footerMeta.posY > this._footerMeta.height)
                    return;
                this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, ' + this._footerMeta.posY + 'px, 0)');
                this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, ' + this._footerMeta.posY + 'px, 0)');
                break;
            case 'panend':
                this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', '300ms ease-in-out');
                if (this._footerMeta.lastPosY > this._footerMeta.posY) {
                    this.state = IonPullUpFooterState.Expanded;
                }
                else if (this._footerMeta.lastPosY < this._footerMeta.posY) {
                    this.state = (this.initialState == IonPullUpFooterState.Minimized) ? IonPullUpFooterState.Minimized : IonPullUpFooterState.Collapsed;
                }
                break;
        }
    };
    IonPullUpComponent.prototype.ngDoCheck = function () {
        var _this = this;
        if (!Object.is(this.state, this._oldState)) {
            switch (this.state) {
                case IonPullUpFooterState.Collapsed:
                    this.collapse();
                    break;
                case IonPullUpFooterState.Expanded:
                    this.expand();
                    break;
                case IonPullUpFooterState.Minimized:
                    this.minimize();
                    break;
            }
            this._oldState = this.state;
            // TODO: fix hack due to BUG (https://github.com/angular/angular/issues/6005)
            window.setTimeout(function () {
                _this.stateChange.emit(_this.state);
            });
        }
    };
    return IonPullUpComponent;
}());

IonPullUpComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */], args: [{
                selector: 'ion-pullup',
                changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectionStrategy */].OnPush,
                template: "\n    <ion-footer #footer>\n      <ng-content></ng-content>\n    </ion-footer>\n    "
            },] },
];
/** @nocollapse */
IonPullUpComponent.ctorParameters = function () { return [
    { type: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */], },
]; };
IonPullUpComponent.propDecorators = {
    'state': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'stateChange': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'initialState': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'defaultBehavior': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'maxHeight': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'onExpand': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'onCollapse': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'onMinimize': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'childFooter': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */], args: ['footer',] },],
};
//# sourceMappingURL=ion-pullup.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonPullUpTabComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/*
ionic-pullup v2 for Ionic/Angular 2
 
Copyright 2016 Ariel Faur (https://github.com/arielfaur)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

var IonPullUpTabComponent = (function () {
    function IonPullUpTabComponent() {
    }
    return IonPullUpTabComponent;
}());

IonPullUpTabComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */], args: [{
                selector: 'ion-pullup-tab',
                template: '<ng-content></ng-content>'
            },] },
];
/** @nocollapse */
IonPullUpTabComponent.ctorParameters = function () { return []; };
//# sourceMappingURL=ion-pullup-tab.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ion_pullup__ = __webpack_require__(357);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__ion_pullup__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ion_pullup_tab__ = __webpack_require__(358);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ion_pullup_module__ = __webpack_require__(360);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__ion_pullup_module__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonPullupModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ion_pullup__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ion_pullup_tab__ = __webpack_require__(358);





var IonPullupModule = (function () {
    function IonPullupModule() {
    }
    return IonPullupModule;
}());

IonPullupModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */], args: [{
                imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */]],
                declarations: [
                    __WEBPACK_IMPORTED_MODULE_3__ion_pullup__["a" /* IonPullUpComponent */],
                    __WEBPACK_IMPORTED_MODULE_4__ion_pullup_tab__["a" /* IonPullUpTabComponent */]
                ],
                exports: [
                    __WEBPACK_IMPORTED_MODULE_3__ion_pullup__["a" /* IonPullUpComponent */],
                    __WEBPACK_IMPORTED_MODULE_4__ion_pullup_tab__["a" /* IonPullUpTabComponent */]
                ],
                providers: [],
                schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
            },] },
];
/** @nocollapse */
IonPullupModule.ctorParameters = function () { return []; };
//# sourceMappingURL=ion-pullup.module.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PRListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PRListPage = /** @class */ (function () {
    function PRListPage(navCtrl, navParams, url, alertCtrl, httpSoap, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.url = url;
        this.alertCtrl = alertCtrl;
        this.httpSoap = httpSoap;
        this.toastCtrl = toastCtrl;
        this.obj = [];
        this.objSearch = [];
        this.userId = navParams.get("param");
        this.count = navParams.get("count");
    }
    PRListPage_1 = PRListPage;
    PRListPage.prototype.ngOnInit = function () {
        var that = this;
        that.url.startLoading().present();
        this.url.checkSession(function (err, resp) {
            if (resp) {
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                that.getPRlist();
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PRListPage.prototype.ionViewDidEnter = function () { };
    PRListPage.prototype.initializeItems = function () {
        this.objSearch = this.obj;
    };
    PRListPage.prototype.doRefresh = function (refresher) {
        this.ngOnInit();
        setTimeout(function () {
            refresher.complete();
        }, 200);
    };
    PRListPage.prototype.getItems = function (ev) {
        this.initializeItems();
        var that = this;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            that.objSearch = that.obj.filter(function (item) {
                return (item.old[0].PurchaseRequisitionRelease[0].PRR_PRNUMBER.toString().toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            console.log("search====", that.objSearch);
        }
    };
    PRListPage.prototype.getPRlist = function () {
        var method = 'GetListOfPRsPending';
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_3_jsencrypt__["JSEncrypt"]();
        var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        that.userId = '';
        that.userId = encrUser;
        that.getPRlistsubCode(parameters, method, that);
        // that.url.forExtraSecurity(function (err, resp) {
        //     if (resp) {
        //         localStorage.setItem("newtm", resp)
        //         var encr = new Encrypt.JSEncrypt();
        //         var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        //         that.userId = '';
        //         that.userId = encrUser;
        //         that.getPRlistsubCode(parameters, method, that);
        //     } else {
        //         console.log("not getting response becz of err: ", err)
        //         that.toastCtrl.create({
        //             message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
        //             position: 'bottom',
        //             duration: 3000
        //         }).present();
        //     }
        // });
    };
    PRListPage.userPRcount = function (username, tk) {
        var parameters = [];
        parameters["userId"] = username;
        parameters["tk"] = tk;
        return parameters;
    };
    PRListPage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    PRListPage.prototype.prDetails = function (data) {
        var that = this;
        this.navCtrl.push('PrDetailsPage', {
            param: data,
            count: that.count
        });
    };
    PRListPage.prototype.getPRlistsubCode = function (parameters, method, that) {
        parameters['GetListOfPRsPending xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PRListPage_1.userPRcount(that.userId, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.url.stopLoading();
                var temp = response;
                var s = temp['SOAP:Envelope'];
                try {
                    var a = s['SOAP:Body'][0].GetListOfPRsPendingResponse;
                    var b = a[0].tuple;
                    if (b == undefined) {
                        var alert_1 = that.alertCtrl.create({
                            message: "No data found..!",
                            buttons: [{
                                    text: 'OK',
                                    handler: function () {
                                        that.navCtrl.setRoot("HomePage");
                                    }
                                }]
                        });
                        alert_1.present();
                    }
                    else {
                        that.obj = b;
                        that.objSearch = b;
                        var formatter = new Intl.NumberFormat('en-US', {
                            minimumFractionDigits: 2,
                        });
                        for (var i = 0; i < that.objSearch.length; i++) {
                            that.objSearch[i].old[0].PurchaseRequisitionRelease[0].fracValue = formatter.format(that.objSearch[i].old[0].PurchaseRequisitionRelease[0].TOTALVALUE); /* $2,500.00 */
                        }
                    }
                }
                catch (e) {
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    var toast = that.toastCtrl.create({
                        message: "Token expired!! Please refresh once..",
                        duration: 2500,
                        position: top
                    });
                    toast.present();
                }
            }
        });
    };
    PRListPage = PRListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-pr-list',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/pr-list/pr-list.html"*/'<ion-header>\n\n  <ion-navbar color="sterlite">\n\n    <ion-title>Purchase Requisitions</ion-title>\n\n  </ion-navbar>\n\n  <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content class="mainContent">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list>\n\n    <ion-item *ngFor="let p of objSearch" (click)="prDetails(p)">\n\n      <ion-row>\n\n        <ion-col col-6>\n\n          <h2>Req # {{p.old[0].PurchaseRequisitionRelease[0].PRR_PRNUMBER}}</h2>\n\n        </ion-col>\n\n        <ion-col col-6 style="text-align: right">\n\n          <h2>{{p.old[0].PurchaseRequisitionRelease[0].LINECOUNT}} Items</h2>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-12 style="text-align: right">\n\n          <p>\n\n            <span style="font-size: 16px; color: black;">{{p.old[0].PurchaseRequisitionRelease[0].PRR_CURRENCY}}</span>&nbsp;\n\n            <span style="font-size: 18px; color: black;">{{p.old[0].PurchaseRequisitionRelease[0].fracValue}}</span>\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="paraHead">\n\n  Sterlite Power\n\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/pr-list/pr-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_http_soap_http_soap__["a" /* HttpSoapProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], PRListPage);
    return PRListPage;
    var PRListPage_1;
}());

//# sourceMappingURL=pr-list.js.map

/***/ })

});
//# sourceMappingURL=0.js.map