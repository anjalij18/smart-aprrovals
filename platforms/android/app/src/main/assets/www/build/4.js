webpackJsonp([4],{

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoDetailsPageModule", function() { return PoDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__po_details__ = __webpack_require__(365);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PoDetailsPageModule = /** @class */ (function () {
    function PoDetailsPageModule() {
    }
    PoDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__po_details__["a" /* PoDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__po_details__["a" /* PoDetailsPage */]),
            ],
        })
    ], PoDetailsPageModule);
    return PoDetailsPageModule;
}());

//# sourceMappingURL=po-details.module.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popover_po_popover_po__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PoDetailsPage = /** @class */ (function () {
    function PoDetailsPage(navCtrl, navParams, popoverCtrl, httpSer, toastCtrl, loadingCtrl, alertCtrl, httpSoap, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.httpSer = httpSer;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.httpSoap = httpSoap;
        this.viewCtrl = viewCtrl;
        this.objhead = [];
        this.objline = [];
        this.docsList = [];
        var dTa = navParams.get('param');
        this.poData = dTa.old[0].STL_TRA_PORELEASE[0];
        this.task_id = this.poData.por_temp3;
    }
    PoDetailsPage_1 = PoDetailsPage;
    PoDetailsPage.prototype.ionViewDidLoad = function () { };
    PoDetailsPage.prototype.ngOnInit = function () {
        var that = this;
        this.httpSer.startLoading().present();
        this.httpSer.checkSession(function (err, resp) {
            if (resp) {
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                that.getPOlistDetails();
            }
            else {
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.httpSer.stopLoadingToast();
            }
        });
        this.getDocsList();
    };
    PoDetailsPage.prototype.goNext = function () {
        var that = this;
        this.navCtrl.push("LineLevelPage", {
            param: that.objline,
            name: that.poData.por_ponumber[0],
            taskId: that.task_id
        });
    };
    PoDetailsPage.prototype.showPopover = function (ev, data) {
        console.log("what is in data: ", data);
        this.popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__popover_po_popover_po__["a" /* PopoverPO */], {
            poData: data
        }, {
            cssClass: 'contact-popover'
        });
        this.popover.onDidDismiss(function () { });
        this.popover.present({
            ev: ev
        });
    };
    PoDetailsPage.prototype.getPOlistDetails = function () {
        var method = 'PO_Mail_Details';
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
        var enrPOnumber = encr.getAddition1(that.poData.por_ponumber);
        var updatedPOnumber = enrPOnumber;
        that.getPOlistDetailssubCode(parameters, method, that, updatedPOnumber);
    };
    PoDetailsPage.userPODetails = function (po_num, tk) {
        var parameters = [];
        parameters["PONumber"] = po_num;
        parameters["tk"] = tk;
        return parameters;
    };
    PoDetailsPage.docsPODetails = function (po_num, tk) {
        var parameters = [];
        parameters["poNumber"] = po_num;
        parameters["tk"] = tk;
        return parameters;
    };
    PoDetailsPage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    PoDetailsPage.prototype.approvePO = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Are you sure you want to proceed?",
            buttons: [{
                    text: 'PROCEED',
                    handler: function () {
                        debugger;
                        var HTTPnew = new XMLHttpRequest();
                        var httpurl = "https://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=" + _this.task_id + "&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve";
                        // const httpurl = 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + this.task_id + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve';
                        HTTPnew.open("GET", httpurl);
                        HTTPnew.send();
                        var toast = _this.toastCtrl.create({
                            message: 'Your request has been processed sucessfully!',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.onDidDismiss(function () {
                            _this.navCtrl.setRoot('HomePage');
                        });
                        toast.present();
                    },
                    cssClass: 'btton'
                },
                {
                    text: 'NO'
                }],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    };
    PoDetailsPage.prototype.rejectPO = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Are you sure you want to proceed?",
            buttons: [{
                    text: 'PROCEED',
                    handler: function () {
                        var HTTPnew = new XMLHttpRequest();
                        var httpurl = "http://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=" + _this.task_id + "&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject";
                        // const httpurl = 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + this.task_id + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject';
                        HTTPnew.open("GET", httpurl);
                        HTTPnew.send();
                        var toast = _this.toastCtrl.create({
                            message: 'Your request has been processed sucessfully!',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.onDidDismiss(function () {
                            _this.navCtrl.setRoot('HomePage');
                        });
                        toast.present();
                    },
                    cssClass: 'btton'
                },
                {
                    text: 'NO'
                }],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    };
    PoDetailsPage.prototype.getPOlistDetailssubCode = function (parameters, method, that, updatedPOnumber) {
        parameters['PO_Mail_Details xmlns="http://schemas.cordys.com/default"'] = PoDetailsPage_1.userPODetails(updatedPOnumber, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.httpSer.stopLoading();
                var temp = response;
                var s = temp['SOAP:Envelope'];
                try {
                    var a = s['SOAP:Body'][0].PO_Mail_DetailsResponse;
                    var head = a[0].PODetails[0].HeaderDetails[0];
                    var line = a[0].PODetails[0].LineDetails[0].Line;
                    that.objhead = head;
                    var formatter = new Intl.NumberFormat('en-US', {
                        minimumFractionDigits: 2,
                    });
                    that.objhead.netvalnum = formatter.format(that.objhead.NetValue); /* $2,500.00 */
                    that.objhead.CreatedOnNew = new Date(that.objhead.CreatedOn).toISOString();
                    that.objline = line;
                    for (var i = 0; i < that.objline.length; i++) {
                        var str = that.objline[i].LineItems.toString();
                        var r = str.replace(/^0+|0+$/, "");
                        that.objline[i].LineItemsNew = r;
                        that.objline[i].showDiv = false;
                        that.objline[i].netvalnum = formatter.format(that.objline[i].NetPrice); /* $2,500.00 */
                    }
                }
                catch (e) {
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    var toast = that.toastCtrl.create({
                        message: "Token expired!! Please refresh once..",
                        duration: 2500,
                        position: 'top'
                    });
                    toast.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.httpSer.stopLoading();
            }
        });
    };
    PoDetailsPage.prototype.getDocsList = function () {
        var that = this;
        that.tk = undefined;
        this.httpSer.checkSession(function (err, resp) {
            if (resp) {
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                that.getDocsListFunc();
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
            }
        });
    };
    PoDetailsPage.prototype.getDocsListFunc = function () {
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
        var enrPOnumber = encr.getAddition1(that.poData.por_ponumber);
        var encryptedPOnum = enrPOnumber;
        that.getDocs(encryptedPOnum, parameters);
    };
    PoDetailsPage.prototype.getDocs = function (encryptedPOnum, parameters) {
        var that = this;
        // parameters['GetPOAvailableAttachments xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PoDetailsPage.docsPODetails(encryptedPOnum, that.tk);
        // parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        parameters['GetPOAvailableAttachmentsFromSAP xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PoDetailsPage_1.docsPODetails(encryptedPOnum, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        this.httpSoap.testService(parameters, function (err, response) {
            debugger;
            if (response) {
                var temp = response;
                var s = temp['SOAP:Envelope'];
                try {
                    // var a = s['SOAP:Body'][0].GetPOAvailableAttachmentsResponse;
                    // var listData = a[0].tuple;
                    // that.docsList = listData;
                    var a = s['SOAP:Body'][0].GetPOAvailableAttachmentsFromSAPResponse;
                    var b = a[0].ZMM_PO_ATTACHMENT_NAMESResponse;
                    var listData = b[0].ET_ATTACHMENT_NAMES;
                    that.docsList = listData;
                    console.log("DOCID : ", that.docsList[0].item[0].DOCID);
                    console.log("FILE_TYPE : ", that.docsList[0].item[0].FILE_TYPE);
                    console.log("FILENAME : ", that.docsList[0].item[0].FILENAME);
                    // console.log("PONUMBER : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].PONUMBER)
                    // console.log("ATTACH_ID : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].ATTACH_ID)
                    // console.log("FILESIZE : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].FILESIZE)
                    // console.log("FILENAME : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].FILENAME)
                }
                catch (e) {
                    var a = s['SOAP:Body'][0]['SOAP:Fault'];
                    var b = a[0].faultstring[0]._;
                    console.log("token err: ", JSON.stringify(b));
                    var toast = that.toastCtrl.create({
                        message: "Token expired!! Please refresh once..",
                        duration: 2500,
                        position: 'top'
                    });
                    toast.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
            }
        });
    };
    PoDetailsPage.prototype.openFileList = function (ev) {
        var that = this;
        console.log("view doc list: ", that.docsList);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__popover_po_popover_po__["a" /* PopoverPO */], {
            poData: that.docsList,
            poNumber: that.poData.por_ponumber
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () { });
        popover.present({
            ev: ev
        });
    };
    PoDetailsPage = PoDetailsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-po-details',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/po-list/po-details/po-details.html"*/'<ion-header>\n\n  <ion-navbar color="sterlite">\n    <ion-title>PO # {{poData.por_ponumber[0]}} ({{objhead.LineCount}})</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="mainContent">\n  <div class="divNew">\n    <ion-row>\n      <ion-col col-4>\n        <b>Plant</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.Plant}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Plant Name</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.PlantName}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Requester</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.Requester}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Partner Name</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.PartnerName}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Payterms</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.PaytermDescription}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Incoterm</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.IncoTerm1}}&nbsp;{{objhead.IncoTerm2}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Value</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.Currency}}&nbsp;{{objhead.netvalnum}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4>\n        <b>Created On</b>\n      </ion-col>\n      <ion-col col-8>{{objhead.CreatedOnNew | date:\'dd-MM-yyyy\'}}</ion-col>\n    </ion-row>\n  \n    <ion-fab top right *ngIf="docsList.length > 0">\n      <button ion-fab mini color="gpsc" (click)="openFileList($event, objhead)">\n        <ion-icon name="document"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n  <ion-list>\n    <ion-item style="border-bottom: 1px solid #0065b3;">\n      <ion-row>\n        <ion-col col-6>\n          <h2>Item(s)</h2>\n        </ion-col>\n        <ion-col col-6 (tap)="(showDiv = !showDiv)" style="text-align: right;">\n          <ion-icon *ngIf="showDiv" name="add" color="sterlite"></ion-icon>\n          <ion-icon *ngIf="!showDiv" name="remove" color="danger"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <div *ngIf="!showDiv">\n      <ion-item *ngFor="let p of objline">\n        <ion-row>\n          <ion-col col-6>\n            <h2>\n              <b>Item # {{p.LineItemsNew}}</b>\n            </h2>\n          </ion-col>\n          <ion-col col-6></ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12>\n            <h3 ion-text tex-wrap>{{p.MaterialDescription}}</h3>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-4>\n            <h3>{{p.Quantity}}&nbsp;{{p.UOM}}</h3>\n          </ion-col>\n          <ion-col col-8 style="text-align: right">\n            <h3>{{p.LineCurrency}}&nbsp;{{p.netvalnum}}</h3>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </div>\n\n  </ion-list>\n</ion-content>\n<ion-footer>\n  <ion-toolbar color="sterlite">\n    <ion-row>\n      <ion-col col-6>\n        <button ion-button block color="secondary" (tap)="approvePO()">Approve</button>\n      </ion-col>\n      <ion-col col-6>\n        <button ion-button block color="danger" (tap)="rejectPO()">Reject</button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/po-list/po-details/po-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__["a" /* HttpSoapProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], PoDetailsPage);
    return PoDetailsPage;
    var PoDetailsPage_1;
}());

//# sourceMappingURL=po-details.js.map

/***/ })

});
//# sourceMappingURL=4.js.map