webpackJsonp([1],{

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(359);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { SoapService } from '../../providers/soap-service/soap-service';
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["b" /* IonPullupModule */]
            ],
            providers: [],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return IonPullUpFooterState; });
/* unused harmony export IonPullUpFooterBehavior */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonPullUpComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/*
ionic-pullup v2 for Ionic/Angular 2
 
Copyright 2016 Ariel Faur (https://github.com/arielfaur)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


var IonPullUpFooterState;
(function (IonPullUpFooterState) {
    IonPullUpFooterState[IonPullUpFooterState["Collapsed"] = 0] = "Collapsed";
    IonPullUpFooterState[IonPullUpFooterState["Expanded"] = 1] = "Expanded";
    IonPullUpFooterState[IonPullUpFooterState["Minimized"] = 2] = "Minimized";
})(IonPullUpFooterState || (IonPullUpFooterState = {}));
var IonPullUpFooterBehavior;
(function (IonPullUpFooterBehavior) {
    IonPullUpFooterBehavior[IonPullUpFooterBehavior["Hide"] = 0] = "Hide";
    IonPullUpFooterBehavior[IonPullUpFooterBehavior["Expand"] = 1] = "Expand";
})(IonPullUpFooterBehavior || (IonPullUpFooterBehavior = {}));
var IonPullUpComponent = (function () {
    function IonPullUpComponent(platform, el, renderer) {
        this.platform = platform;
        this.el = el;
        this.renderer = renderer;
        this.stateChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onExpand = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onCollapse = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onMinimize = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._footerMeta = {
            height: 0,
            posY: 0,
            lastPosY: 0
        };
        this._currentViewMeta = {};
        // sets initial state
        this.initialState = this.initialState || IonPullUpFooterState.Collapsed;
        this.defaultBehavior = this.defaultBehavior || IonPullUpFooterBehavior.Expand;
        this.maxHeight = this.maxHeight || 0;
    }
    IonPullUpComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.debug('ionic-pullup => Initializing footer...');
        window.addEventListener("orientationchange", function () {
            console.debug('ionic-pullup => Changed orientation => updating');
            _this.updateUI();
        });
        this.platform.resume.subscribe(function () {
            console.debug('ionic-pullup => Resumed from background => updating');
            _this.updateUI();
        });
    };
    IonPullUpComponent.prototype.ngAfterContentInit = function () {
        this.computeDefaults();
        this.state = IonPullUpFooterState.Collapsed;
        this.updateUI(true); // need to indicate whether it's first run to avoid emitting events twice due to change detection
    };
    Object.defineProperty(IonPullUpComponent.prototype, "expandedHeight", {
        get: function () {
            return window.innerHeight - this._currentViewMeta.headerHeight;
        },
        enumerable: true,
        configurable: true
    });
    IonPullUpComponent.prototype.computeDefaults = function () {
        this._footerMeta.defaultHeight = this.childFooter.nativeElement.offsetHeight;
        // TODO: still need to test with tabs template (not convinced it is a valid use case...)
        this._currentViewMeta.tabs = this.el.nativeElement.closest('ion-tabs');
        this._currentViewMeta.tabsHeight = this._currentViewMeta.tabs ? this._currentViewMeta.tabs.querySelector('.tabbar').offsetHeight : 0;
        console.debug(this._currentViewMeta.tabsHeight ? 'ionic-pullup => Tabs detected' : 'ionic.pullup => View has no tabs');
        //this._currentViewMeta.hasBottomTabs = this._currentViewMeta.tabs && this._currentViewMeta.tabs.classList.contains('tabs-bottom');
        this._currentViewMeta.header = document.querySelector('ion-navbar.toolbar');
        this._currentViewMeta.headerHeight = this._currentViewMeta.header ? this._currentViewMeta.header.offsetHeight : 0;
    };
    IonPullUpComponent.prototype.computeHeights = function (isInit) {
        if (isInit === void 0) { isInit = false; }
        this._footerMeta.height = this.maxHeight > 0 ? this.maxHeight : this.expandedHeight;
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'height', this._footerMeta.height + 'px');
        // TODO: implement minimize mode
        //this.renderer.setElementStyle(this.el.nativeElement, 'min-height', this._footerMeta.height + 'px'); 
        //if (this.initialState == IonPullUpFooterState.Minimized) {
        //  this.minimize()  
        //} else {
        this.collapse(isInit);
        //} 
    };
    IonPullUpComponent.prototype.updateUI = function (isInit) {
        var _this = this;
        if (isInit === void 0) { isInit = false; }
        setTimeout(function () {
            _this.computeHeights(isInit);
        }, 300);
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', 'none'); // avoids flickering when changing orientation
    };
    IonPullUpComponent.prototype.expand = function () {
        this._footerMeta.lastPosY = 0;
        this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, 0, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, 0, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', '300ms ease-in-out');
        this.onExpand.emit(null);
    };
    IonPullUpComponent.prototype.collapse = function (isInit) {
        if (isInit === void 0) { isInit = false; }
        this._footerMeta.lastPosY = this._footerMeta.height - this._footerMeta.defaultHeight - this._currentViewMeta.tabsHeight;
        this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        if (!isInit)
            this.onCollapse.emit(null);
    };
    IonPullUpComponent.prototype.minimize = function () {
        this._footerMeta.lastPosY = this._footerMeta.height;
        this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, ' + this._footerMeta.lastPosY + 'px, 0)');
        this.onMinimize.emit(null);
    };
    IonPullUpComponent.prototype.onTap = function (e) {
        e.preventDefault();
        if (this.state == IonPullUpFooterState.Collapsed) {
            if (this.defaultBehavior == IonPullUpFooterBehavior.Hide)
                this.state = IonPullUpFooterState.Minimized;
            else
                this.state = IonPullUpFooterState.Expanded;
        }
        else {
            if (this.state == IonPullUpFooterState.Minimized) {
                if (this.defaultBehavior == IonPullUpFooterBehavior.Hide)
                    this.state = IonPullUpFooterState.Collapsed;
                else
                    this.state = IonPullUpFooterState.Expanded;
            }
            else {
                // footer is expanded
                this.state = this.initialState == IonPullUpFooterState.Minimized ? IonPullUpFooterState.Minimized : IonPullUpFooterState.Collapsed;
            }
        }
    };
    IonPullUpComponent.prototype.onDrag = function (e) {
        e.preventDefault();
        switch (e.type) {
            case 'panstart':
                this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', 'none');
                break;
            case 'pan':
                this._footerMeta.posY = Math.round(e.deltaY) + this._footerMeta.lastPosY;
                if (this._footerMeta.posY < 0 || this._footerMeta.posY > this._footerMeta.height)
                    return;
                this.renderer.setElementStyle(this.childFooter.nativeElement, '-webkit-transform', 'translate3d(0, ' + this._footerMeta.posY + 'px, 0)');
                this.renderer.setElementStyle(this.childFooter.nativeElement, 'transform', 'translate3d(0, ' + this._footerMeta.posY + 'px, 0)');
                break;
            case 'panend':
                this.renderer.setElementStyle(this.childFooter.nativeElement, 'transition', '300ms ease-in-out');
                if (this._footerMeta.lastPosY > this._footerMeta.posY) {
                    this.state = IonPullUpFooterState.Expanded;
                }
                else if (this._footerMeta.lastPosY < this._footerMeta.posY) {
                    this.state = (this.initialState == IonPullUpFooterState.Minimized) ? IonPullUpFooterState.Minimized : IonPullUpFooterState.Collapsed;
                }
                break;
        }
    };
    IonPullUpComponent.prototype.ngDoCheck = function () {
        var _this = this;
        if (!Object.is(this.state, this._oldState)) {
            switch (this.state) {
                case IonPullUpFooterState.Collapsed:
                    this.collapse();
                    break;
                case IonPullUpFooterState.Expanded:
                    this.expand();
                    break;
                case IonPullUpFooterState.Minimized:
                    this.minimize();
                    break;
            }
            this._oldState = this.state;
            // TODO: fix hack due to BUG (https://github.com/angular/angular/issues/6005)
            window.setTimeout(function () {
                _this.stateChange.emit(_this.state);
            });
        }
    };
    return IonPullUpComponent;
}());

IonPullUpComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */], args: [{
                selector: 'ion-pullup',
                changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectionStrategy */].OnPush,
                template: "\n    <ion-footer #footer>\n      <ng-content></ng-content>\n    </ion-footer>\n    "
            },] },
];
/** @nocollapse */
IonPullUpComponent.ctorParameters = function () { return [
    { type: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], },
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */], },
]; };
IonPullUpComponent.propDecorators = {
    'state': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'stateChange': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'initialState': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'defaultBehavior': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'maxHeight': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */] },],
    'onExpand': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'onCollapse': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'onMinimize': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */] },],
    'childFooter': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */], args: ['footer',] },],
};
//# sourceMappingURL=ion-pullup.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonPullUpTabComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/*
ionic-pullup v2 for Ionic/Angular 2
 
Copyright 2016 Ariel Faur (https://github.com/arielfaur)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

var IonPullUpTabComponent = (function () {
    function IonPullUpTabComponent() {
    }
    return IonPullUpTabComponent;
}());

IonPullUpTabComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */], args: [{
                selector: 'ion-pullup-tab',
                template: '<ng-content></ng-content>'
            },] },
];
/** @nocollapse */
IonPullUpTabComponent.ctorParameters = function () { return []; };
//# sourceMappingURL=ion-pullup-tab.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ion_pullup__ = __webpack_require__(357);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__ion_pullup__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ion_pullup_tab__ = __webpack_require__(358);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ion_pullup_module__ = __webpack_require__(360);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__ion_pullup_module__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonPullupModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ion_pullup__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ion_pullup_tab__ = __webpack_require__(358);





var IonPullupModule = (function () {
    function IonPullupModule() {
    }
    return IonPullupModule;
}());

IonPullupModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */], args: [{
                imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */]],
                declarations: [
                    __WEBPACK_IMPORTED_MODULE_3__ion_pullup__["a" /* IonPullUpComponent */],
                    __WEBPACK_IMPORTED_MODULE_4__ion_pullup_tab__["a" /* IonPullUpTabComponent */]
                ],
                exports: [
                    __WEBPACK_IMPORTED_MODULE_3__ion_pullup__["a" /* IonPullUpComponent */],
                    __WEBPACK_IMPORTED_MODULE_4__ion_pullup_tab__["a" /* IonPullUpTabComponent */]
                ],
                providers: [],
                schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
            },] },
];
/** @nocollapse */
IonPullupModule.ctorParameters = function () { return []; };
//# sourceMappingURL=ion-pullup.module.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    function LoginPage(url, navCtrl, navParams, formBuilder, events, alertCtrl, toastCtrl, httpSoap) {
        this.url = url;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.httpSoap = httpSoap;
        this.startText = "LOGIN";
        this.isUnchanged = false;
        this.chVal = 10;
        this.isUnchanged1 = false;
        this.username = '';
        this.password = '';
        this.footerState = __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.loginForm = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].email],
            password: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
        });
        this.signupForm = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].email]
        });
        this.startText = "LOGIN";
        if (localStorage.getItem("EmailIdEntered") != null) {
            var emailSTr = localStorage.getItem("EmailIdEntered");
            this.loginForm.patchValue({ 'email': emailSTr });
        }
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewDidLoad = function () {
        var test = localStorage.getItem('footer');
        if (test == 1) {
            this.footerState2 = 0;
        }
    };
    LoginPage.prototype.ionViewDidEnter = function () { };
    LoginPage.prototype.footerExpanded = function () { };
    LoginPage.prototype.footerCollapsed = function () {
        if (this.footerState == 1) {
            this.footerState = 0;
        }
        else if (this.footerState == 1) {
            this.footerState = 1;
        }
    };
    LoginPage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    LoginPage.prototype.logForm = function () {
        if (this.loginForm.valid) {
            this.isUnchanged1 = true;
            var res = this.loginForm.value.email.split("@");
            var userid = res[0];
            var method = 'AuthenticateStlUser';
            var parameters = [];
            this.username = userid;
            this.password = this.loginForm.value.password;
            var that_1 = this;
            localStorage.setItem("userId", that_1.username);
            this.url.startLoading().present();
            this.url.forExtraSecurity(function (err, resp) {
                if (resp) {
                    localStorage.setItem("newtm", resp);
                    var encr = new __WEBPACK_IMPORTED_MODULE_5_jsencrypt__["JSEncrypt"]();
                    that_1.username = encr.getAddition1(that_1.username);
                    that_1.password = encr.getAddition2(that_1.password);
                    that_1.callSubCode(parameters, method);
                }
                else {
                    console.log("not getting response becz of err: ", err);
                    that_1.toastCtrl.create({
                        message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                        position: 'bottom',
                        duration: 3000
                    }).present();
                    that_1.isUnchanged1 = false;
                    that_1.url.stopLoading();
                }
            });
        }
    };
    LoginPage.prototype.callSubCode = function (parameters, method) {
        var that = this;
        parameters['AuthenticateStlUser xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = LoginPage_1.userLogin(that.username, that.password);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            that.url.stopLoading();
            debugger;
            if (response) {
                var s = response['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].AuthenticateStlUserResponse;
                var b = a[0].tuple[0].old[0].authenticateStlUser[0].authenticateStlUser[0];
                console.log("login debugger:", b);
                if (b == "INVALID CREDENTIALS/SAP ID Not Exists") {
                    that.toastCtrl.create({
                        message: b,
                        duration: 2000,
                        position: 'bottom'
                    }).present();
                    that.isUnchanged1 = false;
                    // that.navCtrl.setRoot('LoginPage');
                }
                else {
                    var strstr = b.split('#');
                    var str1 = strstr[3];
                    var str2 = str1.split(':');
                    var pocount = str2[1];
                    var str3 = strstr[4];
                    var str4 = str3.split(':');
                    var prcount = str4[1];
                    var toast = that.toastCtrl.create({
                        message: 'You have logged in successfully',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.events.publish("user:set", b);
                        localStorage.setItem("userName", b);
                        localStorage.setItem("oldPass", that.loginForm.value.password);
                        localStorage.setItem("LOGGEDIN", "LOGGEDIN");
                        localStorage.setItem("EmailIdEntered", that.loginForm.value.email);
                        that.navCtrl.setRoot('HomePage', {
                            "pocount_param": pocount,
                            "prcount_param": prcount
                        });
                    });
                    toast.present();
                }
            }
            else {
                console.log("error found in err tag: ", err);
                console.log("no response cought");
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.isUnchanged1 = false;
            }
        });
    };
    LoginPage.userLogin = function (username, password) {
        var parameters = [];
        parameters["userId"] = username;
        parameters["password"] = password;
        return parameters;
    };
    LoginPage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    LoginPage.prototype.signup = function () {
        var signupFormEmail = this.signupForm.get('email');
        var loginFormEmail = this.loginForm.get('email');
        signupFormEmail.setValue(loginFormEmail.value);
        var that = this;
        if (that.footerState1 == 0) {
            that.footerState1 = 1;
        }
        else {
            if (that.footerState1 == 1) {
                that.footerState = 0;
                that.footerState1 = 0;
            }
        }
    };
    LoginPage.prototype.login = function () {
        var that = this;
        that.footerState1 = 0;
        that.startText = "LOGIN";
        that.footerState = 1;
    };
    LoginPage.prototype.login2 = function () {
        var that = this;
        that.footerState2 = 0;
        that.footerState1 = 0;
        that.startText = "LOGIN";
        that.footerState = 0;
    };
    LoginPage.prototype.doSignUp = function () {
        var _this = this;
        this.signinObj = this.signupForm.value.email;
        localStorage.setItem('userdetail', this.signinObj);
        localStorage.setItem("EmailIdEntered", this.signupForm.value.email);
        if (this.signinObj) {
            this.isUnchanged = true;
            var str = this.signupForm.value.email.split('@');
            var aft = str[1];
            if (aft == 'sterlite.com' || aft == 'adnatesolutions.com' || aft == 'oneqlik.in') {
                var method = 'CreateSTLAuthUserOTP';
                var parameters = [];
                // debugger;
                this.username = (this.signupForm.value.email).toString().toLowerCase();
                this.password_length = "4";
                this.validity = "30";
                var that_2 = this;
                this.url.forExtraSecurity(function (err, resp) {
                    if (resp) {
                        console.log("resp from callback to login screen=> ", resp);
                        localStorage.setItem("newtm", resp);
                        var encr = new __WEBPACK_IMPORTED_MODULE_5_jsencrypt__["JSEncrypt"]();
                        encr.getAddition1(that_2.username);
                        that_2.username = encr.getAddition1(that_2.username);
                        // that.password = encr.getAddition2(that.password);
                        console.log("encrypted signup username: ", that_2.username);
                        console.log("encrypted signup password: ", that_2.password_length);
                        that_2.signupSubFunc(parameters, method);
                    }
                    else {
                        console.log("not getting response becz of err: ", err);
                        that_2.toastCtrl.create({
                            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                            position: 'bottom',
                            duration: 3000
                        }).present();
                        // that.isUnchanged1 = false;
                        that_2.isUnchanged = false;
                    }
                });
                // that.signupSubFunc(parameters, method);
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    message: 'Please provide valid Sterlite e-mail id.',
                    buttons: [{
                            text: 'Okay',
                            handler: function () {
                                _this.signupForm.reset();
                                _this.isUnchanged = false;
                            }
                        }]
                });
                alert_1.present();
            }
        }
    };
    LoginPage.userSignup = function (username, password, validity) {
        var parameters = [];
        parameters["EMAIL_ID"] = username;
        parameters["PWD_LENGTH"] = password;
        parameters["VALIDITY"] = validity;
        return parameters;
    };
    LoginPage.prototype.signupSubFunc = function (parameters, method) {
        parameters['CreateSTLAuthUserOTP  xmlns="http://schemas.cordys.com/default"'] = LoginPage_1.userSignup(this.username, this.password_length, this.validity);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        var that = this;
        this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            that.url.stopLoading();
            if (response) {
                console.log("otp response: ", response);
                debugger;
                var s = response['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].CreateSTLAuthUserOTPResponse;
                var b = a[0].OUTMSG[0]._;
                if (b == 'Your OTP has been sent on your e-mail.') {
                    var toast = that.toastCtrl.create({
                        message: b,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.signupForm.value.email = "";
                        that.navCtrl.setRoot("OtPpagePage", {
                            "param": that.username
                        });
                        that.footerState1 = 1;
                    });
                    toast.present();
                }
                else {
                    var toast = that.toastCtrl.create({
                        message: b,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.signupForm.value.email = "";
                        that.navCtrl.setRoot("LoginPage");
                        that.footerState1 = 1;
                    });
                    toast.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.isUnchanged = false;
            }
        });
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/login/login.html"*/'<ion-content class="bgClass">\n  <div class="logoDiv">\n    <img src="assets/imgs/icon.png" />\n    <!-- <p style="letter-spacing: 2px; color: #0065b3; font-size: 1.3em; font-weight: thin;">STERLITE POWER</p> -->\n  </div>\n</ion-content>\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState" *ngIf="startText === \'LOGIN\'">\n  <ion-toolbar color="sterlite" (tap)="toggleFooter()" *ngIf="footerState == 0">\n    <p align="center" style="color: white; font-size:18px; letter-spacing: 3px;">LOGIN</p>\n  </ion-toolbar>\n  <ion-content class="contentTwo">\n    <form [formGroup]="loginForm">\n      <ion-item>\n        <ion-label color="light" floating>E-MAIL</ion-label>\n        <!-- <ion-input type="text" formControlName="email" (keydown.space)="$event.preventDefault();"></ion-input> -->\n        <ion-input type="text" formControlName="email"></ion-input>\n\n      </ion-item>\n      <ion-item>\n        <ion-label color="light" floating>PASSWORD</ion-label>\n        <ion-input type="password" formControlName="password"></ion-input>\n      </ion-item>\n      <div style="text-align: center; padding-top: 25px;">\n        <button ion-button class="buttonClass" [disabled]="!loginForm.valid || isUnchanged1" (tap)="logForm()">LOGIN</button>\n        <!-- <button ion-button class="buttonClass" [disabled]="!loginForm.valid" (tap)="logForm()">LOGIN</button> -->\n      </div>\n      <div style="padding-top: 20px; text-align: center">\n        <div class="btnClass" (tap)="signup()">Generate Password</div>\n       \n      </div>\n    </form>\n  </ion-content>\n</ion-pullup>\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState1">\n  <!-- <ion-toolbar color="sterlite" (tap)="toggleFooter()" *ngIf="footerState == 0">\n    <p align="center" style="color: white; font-size:18px; letter-spacing: 2px;">Generate Password</p>\n  </ion-toolbar> -->\n  <ion-content class="contentTwo">\n    <form [formGroup]="signupForm" padding-top >\n      <ion-item>\n        <ion-label color="light" floating>E-MAIL</ion-label>\n        <!-- <ion-input type="text" formControlName="email" (keydown.space)="$event.preventDefault();" required></ion-input> -->\n        <ion-input type="text" formControlName="email" required></ion-input>\n\n      </ion-item>\n      <div style="text-align: center; padding-top: 25px;">\n        <!-- <button ion-button class="buttonClass1" [disabled]="!signupForm.valid || isUnchanged" (tap)="doSignUp()">Generate Password</button> -->\n        <button ion-button class="buttonClass1" [disabled]="!signupForm.valid || isUnchanged" (tap)="doSignUp()">Generate OTP</button>\n      </div>\n      <div style="padding-top: 20px; text-align: center">\n        <div style="color: white; font-size: 15px; letter-spacing: 2px">Already logged in?\n          <div class="btnClass" (tap)="login()">LOGIN</div>\n        </div>\n      </div>\n    </form>\n\n   \n  </ion-content>\n</ion-pullup>\n\n'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_http_soap_http_soap__["a" /* HttpSoapProvider */]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=1.js.map