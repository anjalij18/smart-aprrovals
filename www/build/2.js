webpackJsonp([2],{

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrDetailsPageModule", function() { return PrDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pr_details__ = __webpack_require__(367);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { PopoverPR } from './popover-pr/popover-pr';
var PrDetailsPageModule = /** @class */ (function () {
    function PrDetailsPageModule() {
    }
    PrDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pr_details__["a" /* PrDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pr_details__["a" /* PrDetailsPage */]),
            ],
            entryComponents: []
        })
    ], PrDetailsPageModule);
    return PrDetailsPageModule;
}());

//# sourceMappingURL=pr-details.module.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popover_pr_popover_pr__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PrDetailsPage = /** @class */ (function () {
    function PrDetailsPage(httpSer, navCtrl, navParams, popoverCtrl, toastCtrl, url, alertCtrl, httpSoap) {
        this.httpSer = httpSer;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.toastCtrl = toastCtrl;
        this.url = url;
        this.alertCtrl = alertCtrl;
        this.httpSoap = httpSoap;
        this.obj = [];
        this.lineApp = '';
        this.submitBtnShow = false;
        var dTa = navParams.get('param');
        this.count = dTa.old[0].PurchaseRequisitionRelease[0].LINECOUNT[0];
        this.poData = dTa.old[0].PurchaseRequisitionRelease[0];
        this.prnum = this.poData.PRR_PRNUMBER[0];
    }
    PrDetailsPage_1 = PrDetailsPage;
    PrDetailsPage.prototype.ionViewDidLoad = function () { };
    PrDetailsPage.prototype.ionViewDidEnter = function () { };
    PrDetailsPage.prototype.ngOnInit = function () {
        var that = this;
        this.url.startLoading().present();
        this.url.checkSession(function (err, resp) {
            if (resp) {
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                that.getPRlistDetails();
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PrDetailsPage.prototype.getPRlistDetails = function () {
        var method = 'GetDetailsOfPR';
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
        var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
        var updatedPRnum = encrPR;
        that.getPRlistDetailssubCode(parameters, method, that, updatedPRnum);
        // that.url.forExtraSecurity(function (err, resp) {
        //   if (resp) {
        //     localStorage.setItem("newtm", resp)
        //     var encr = new Encrypt.JSEncrypt();
        //     var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
        //     var updatedPRnum = encrPR;
        //     that.getPRlistDetailssubCode(parameters, method, that, updatedPRnum);
        //   } else {
        //     console.log("not getting response becz of err: ", err)
        //     that.toastCtrl.create({
        //       message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
        //       position: 'bottom',
        //       duration: 3000
        //     }).present();
        //     that.url.stopLoading();
        //   }
        // });
    };
    PrDetailsPage.userPRcount = function (pr_num, rel_code, rel_group, tk) {
        var parameters = [];
        parameters["prNumber"] = pr_num;
        parameters["relCode"] = rel_code;
        parameters["relGroup"] = rel_group;
        parameters["tk"] = tk;
        return parameters;
    };
    PrDetailsPage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    PrDetailsPage.prototype.showPopoverPR = function (ev, data) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__popover_pr_popover_pr__["a" /* PopoverPR */], {
            prData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () { });
        popover.present({
            ev: ev
        });
    };
    PrDetailsPage.prototype.rejectPR = function () {
        debugger;
        var that = this;
        var parameters = [];
        this.url.checkSession(function (err, resp) {
            if (resp) {
                that.tk = "";
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
                var updatedPRnum = encrPR;
                that.rejectPRFunc(updatedPRnum, that.tk, parameters);
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PrDetailsPage.prototype.rejectPRFunc = function (updatedPRnum, tkToken, parameters) {
        var method = 'UpdatePRApprovals';
        // var parameters: any;
        parameters['UpdatePRApprovals xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage_1.userPRApprove(updatedPRnum, this.poData.PRR_RELEASECODE, this.poData.PRR_RELEASEGROUP, this.typeAR, '', '', 'R', tkToken);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        var that = this;
        this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.url.stopLoading();
                var temp = response;
                var s = temp['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].UpdatePRApprovalsResponse;
                var b = a[0].return;
                if (b == "true") {
                    var toast = that.toastCtrl.create({
                        message: 'Your request is processed!',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.navCtrl.setRoot('HomePage');
                    });
                    toast.present();
                }
                else {
                    var toast = that.toastCtrl.create({
                        message: 'Something went wrong!',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                    });
                    toast.present();
                }
            }
        });
    };
    PrDetailsPage.prototype.approvePR = function () {
        debugger;
        var that = this;
        var parameters = [];
        this.url.checkSession(function (err, resp) {
            if (resp) {
                that.tk = "";
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
                var updatedPRnum = encrPR;
                that.approvePRFunc(updatedPRnum, that.tk, parameters);
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PrDetailsPage.prototype.approvePRFunc = function (updatedPRnum, tkToken, parameters) {
        var method = 'UpdatePRApprovals';
        // var parameters: any;
        parameters['UpdatePRApprovals xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage_1.userPRApprove(updatedPRnum, this.poData.PRR_RELEASECODE, this.poData.PRR_RELEASEGROUP, this.typeAR, '', '', 'A', tkToken);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        var that = this;
        this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            that.url.stopLoading();
            if (response) {
                var temp = response;
                var s = temp['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].UpdatePRApprovalsResponse;
                var b = a[0].return;
                if (b == "true") {
                    var toast = that.toastCtrl.create({
                        message: 'Your request is processed!',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        console.log('Dismissed toast');
                        that.navCtrl.setRoot('HomePage');
                    });
                    toast.present();
                }
                else {
                    var toast = that.toastCtrl.create({
                        message: 'Something went wrong!',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        console.log('Dismissed toast');
                        // that.navCtrl.setRoot('HomePage');
                    });
                    toast.present();
                }
            }
        });
    };
    PrDetailsPage.userPRApprove = function (pr_num, rel_code, rel_group, typeApprove, lineApprove, headapcomm, headAprrove, tk) {
        debugger;
        var parameters = [];
        parameters["prNumber"] = pr_num;
        parameters["relCode"] = rel_code;
        parameters["relGroup"] = rel_group;
        parameters["typeOfApproval"] = typeApprove;
        parameters["lineApprovals"] = lineApprove;
        parameters["headerApprovalComments"] = headapcomm;
        parameters["headerApproval"] = headAprrove;
        parameters["tk"] = tk;
        return parameters;
    };
    PrDetailsPage.prototype.approveFunc = function (p) {
        p.tax = "approve";
        p.approvebuttonColor = '#32db64';
        p.rejectbuttonColor = '#8f8f91';
        p.comment = undefined;
        this.radioChecked(p);
    };
    PrDetailsPage.prototype.rejectFunc = function (p) {
        p.tax = "reject";
        p.approvebuttonColor = '#8f8f91';
        p.rejectbuttonColor = '#f53d3d';
        this.radioChecked(p);
    };
    PrDetailsPage.prototype.radioChecked = function (p) {
        if (p.tax == "approve") {
            if (p.showBTN == true) {
                p.showBTN = false;
            }
            var that = this;
            var lineno = p.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
            that.lineApp = that.lineApp + lineno + "@@@A@@@NOCOMMENTS###";
            var pattern = new RegExp(lineno + "@@@R@@@[a-zA-Z0-9]*###");
            that.lineApp = that.lineApp.replace(pattern, "");
            this.comment = undefined;
            p.values = undefined;
        }
        else {
            if (p.tax == "reject") {
                p.showBTN = true;
                var that = this;
                var lineno = p.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
                that.lineApp = that.lineApp.replace(lineno + "@@@A@@@NOCOMMENTS###", "");
                that.lineApp = that.lineApp + lineno + "@@@R@@@NOCOMMENTS###";
                this.comment = undefined;
            }
        }
    };
    PrDetailsPage.prototype.addComment = function (comm, data, i) {
        data.values = comm;
        var that = this;
        var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
        that.lineApp = that.lineApp.replace(lineno + "@@@R@@@NOCOMMENTS###", "");
        that.lineApp = that.lineApp + lineno + "@@@R@@@" + data.comment + "###";
        data.showBTN = false;
        data.comment = undefined;
    };
    PrDetailsPage.prototype.lineApprove = function (data) {
        this.tempVar = "SET";
        var that = this;
        if (data.approve) {
            var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
            that.lineApp = that.lineApp + lineno + "@@@A@@@NOCOMMENTS###";
        }
        else {
            var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
            that.lineApp = that.lineApp.replace(lineno + "@@@A@@@NOCOMMENTS###", "");
        }
    };
    PrDetailsPage.prototype.lineReject = function (data) {
        this.tempVar = "SET";
        var that = this;
        var lineno = data.old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER[0];
        that.lineApp = that.lineApp + lineno + "@@@R@@@" + that.comment + "###";
    };
    PrDetailsPage.prototype.submitApproval = function () {
        var method = 'UpdatePRApprovals';
        var parameters = [];
        var that = this;
        this.url.startLoading().present();
        this.url.checkSession(function (err, resp) {
            if (resp) {
                that.tk = "";
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
                var updatedPRnum = encrPR;
                that.submitAppsubCode(parameters, method, that, updatedPRnum, that.tk);
                // that.url.forExtraSecurity(function (err, resp) {
                //   if (resp) {
                //     localStorage.setItem("newtm", resp)
                //     var encr = new Encrypt.JSEncrypt();
                //     var encrPR = encr.getAddition1(that.poData.PRR_PRNUMBER);
                //     var updatedPRnum = encrPR;
                //     that.submitAppsubCode(parameters, method, that, updatedPRnum, that.tk);
                //   } else {
                //     console.log("not getting response becz of err: ", err)
                //     that.toastCtrl.create({
                //       message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                //       position: 'bottom',
                //       duration: 3000
                //     }).present();
                //     that.url.stopLoading();
                //   }
                // });
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PrDetailsPage.prototype.getPRlistDetailssubCode = function (parameters, method, that, updatedPRnum) {
        parameters['GetDetailsOfPR xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage_1.userPRcount(updatedPRnum, that.poData.PRR_RELEASECODE, that.poData.PRR_RELEASEGROUP, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.url.stopLoading();
                var temp = response;
                var s = temp['SOAP:Envelope'];
                if (s['SOAP:Body'] == undefined) {
                    var alert_1 = that.alertCtrl.create({
                        message: 'Something went wrong. Please visit after some time.',
                        buttons: [{
                                text: 'Okay'
                            }]
                    });
                    alert_1.present();
                }
                else {
                    try {
                        var a = s['SOAP:Body'][0].GetDetailsOfPRResponse;
                        var b = a[0].tuple;
                        debugger;
                        for (var i = 0; i < b.length; i++) {
                            b.approve = false;
                            b.reject = false;
                            b.tax = [];
                            b.approvebuttonColor = "#8f8f91";
                            b.rejectbuttonColor = "#8f8f91";
                            b.values = [];
                            b.showBTN = false;
                            b.comment = undefined;
                        }
                        that.obj = b;
                        var formatter = new Intl.NumberFormat('en-US', {
                            minimumFractionDigits: 2,
                        });
                        for (var i = 0; i < that.obj.length; i++) {
                            var str = that.obj[i].old[0].PurchaseRequisitionRelease[0].PRR_LINENUMBER.toString();
                            var r = str.replace(/^0+|0+$/, "");
                            that.obj[i].old[0].PurchaseRequisitionRelease[0].LineItemsNew = r;
                            that.obj[i].old[0].PurchaseRequisitionRelease[0].isoDate = new Date(that.obj[i].old[0].PurchaseRequisitionRelease[0].PRRLINE_CREATED_DATE).toISOString();
                            that.obj[i].old[0].PurchaseRequisitionRelease[0].fracValue = formatter.format(that.obj[i].old[0].PurchaseRequisitionRelease[0].PRR_NET_VALUE); /* $2,500.00 */
                        }
                        that.typeAR = that.obj[0].old[0].PurchaseRequisitionRelease[0].PR_REL_TYPE[0];
                        if (that.obj[0].old[0].PurchaseRequisitionRelease[0].PR_REL_TYPE[0] == 'h') {
                            that.typeAR = that.obj[0].old[0].PurchaseRequisitionRelease[0].PR_REL_TYPE[0];
                        }
                        else {
                            that.typeAR = 'l';
                        }
                    }
                    catch (e) {
                        var a = s['SOAP:Body'][0]['SOAP:Fault'];
                        var b = a[0].faultstring[0]._;
                        var toast = that.toastCtrl.create({
                            message: "Token expired!! Please refresh once..",
                            duration: 2500,
                            position: top
                        });
                        toast.present();
                    }
                }
            }
            else {
                that.url.stopLoading();
            }
        });
    };
    PrDetailsPage.prototype.submitAppsubCode = function (parameters, method, that, updatedPRnum, tkToken) {
        parameters['UpdatePRApprovals xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PrDetailsPage_1.userPRApprove(updatedPRnum, that.poData.PRR_RELEASECODE, that.poData.PRR_RELEASEGROUP, that.typeAR, that.lineApp, '', '', tkToken);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                that.url.stopLoading();
                var temp = response;
                var s = temp['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].UpdatePRApprovalsResponse;
                var b = a[0].return;
                if (b == "true") {
                    var toast = that.toastCtrl.create({
                        message: 'Your request has been processed sucessfully!',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () {
                        that.navCtrl.setRoot('HomePage');
                    });
                    toast.present();
                }
                else {
                    var toast = that.toastCtrl.create({
                        message: 'Something went wrong!',
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(function () { });
                    toast.present();
                }
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        });
    };
    PrDetailsPage = PrDetailsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-pr-details',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/pr-list/pr-details/pr-details.html"*/'<ion-header>\n\n  <ion-navbar color="sterlite">\n    <ion-title>Req # {{prnum}} ({{count}})</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="mainContent">\n  <div class="divNew" *ngIf="typeAR == \'l\'">\n    <ion-list>\n      <div padding *ngFor="let p of obj; let i = index;" class="divClass">\n        <ion-row>\n          <ion-col col-6>\n            <b>Item # {{p.old[0].PurchaseRequisitionRelease[0].LineItemsNew}}</b>\n          </ion-col>\n          <ion-col col-6 style="text-align: right">\n            <b>{{p.old[0].PurchaseRequisitionRelease[0].isoDate | date:\'dd-MM-yyyy\'}}</b>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12 ion-text text-wrap>\n            {{p.old[0].PurchaseRequisitionRelease[0].PRR_MATERIAL_DESC}}\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-4>\n            {{p.old[0].PurchaseRequisitionRelease[0].PRR_QUANTITY}}&nbsp;\n            <span\n              *ngIf="!p.old[0].PurchaseRequisitionRelease[0].PR_LINE_UOM[0].$">{{p.old[0].PurchaseRequisitionRelease[0].PR_LINE_UOM}}</span>\n          </ion-col>\n\n          <ion-col col-8 style="text-align: right">\n            {{p.old[0].PurchaseRequisitionRelease[0].PRR_CURRENCY}}&nbsp;\n            <span style="font-size: 18px;">{{p.old[0].PurchaseRequisitionRelease[0].fracValue}}</span>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12>\n            Requisitioner:&nbsp;\n            <span\n              *ngIf="!p.old[0].PurchaseRequisitionRelease[0].PRR_CREATEDBY[0].$">{{p.old[0].PurchaseRequisitionRelease[0].PRR_CREATEDBY}}</span>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == \'3R\'">\n          <ion-col col-12 *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_COMMEMTS != \'[object Object]\'">\n            <b>Comments</b>:&nbsp;{{p.old[0].PurchaseRequisitionRelease[0].PRR_COMMEMTS}}\n          </ion-col>\n          <ion-col col-12 *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_COMMEMTS == \'[object Object]\'">\n            <b>Comments</b>:&nbsp;N/A\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf="p.values">\n          <ion-col col-12>\n            <b>Comments</b>:&nbsp;{{p.values}}\n          </ion-col>\n        </ion-row>\n\n        <ion-row style="padding-left: 0px !important;"\n          *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == \'3A\' || p.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == \'4\'">\n          <ion-col col-4>\n            <button ion-button small round color="secondary" [disabled]="true">Approve</button>\n          </ion-col>\n          <ion-col col-2>\n            <img src="assets/imgs/right-image-png-4.png" height="20" width="20"\n              style="opacity: 0.5; margin-left: 4px; margin-top: 8px;" />\n          </ion-col>\n          <ion-col col-6 style="text-align: right">\n            <button ion-button small round color="grey" [disabled]="true">Reject</button>\n          </ion-col>\n        </ion-row>\n        <ion-row style="padding-left: 0px !important;"\n          *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == \'3R\'">\n          <ion-col col-6>\n            <button ion-button small round color="grey" [disabled]="true">Approve</button>\n          </ion-col>\n          <ion-col col-2 style="text-align: right">\n            <img src="assets/imgs/New Project.png" height="20" width="20"\n              style="opacity: 0.5; margin-top: 8px; margin-right: -12px" />\n          </ion-col>\n          <ion-col col-4 style="text-align: right">\n            <button ion-button small round color="danger" [disabled]="true">Reject</button>\n          </ion-col>\n        </ion-row>\n\n        <div *ngIf="p.tax != \'reject\' && p.tax != \'approve\'">\n          <ion-row style="padding-left: 0px !important;" *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == 1">\n            <ion-col col-6>\n              <button ion-button small round color="secondary" (tap)="approveFunc(p);"\n                [ngStyle]="{\'background-color\': p.approvebuttonColor}">Approve</button>\n            </ion-col>\n            <ion-col col-6 style="text-align: right">\n              <button ion-button small round color="danger" (tap)="rejectFunc(p);"\n                [ngStyle]="{\'background-color\': p.rejectbuttonColor}">Reject</button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div *ngIf="p.tax == \'reject\' || p.tax == \'approve\'">\n          <ion-row style="padding-left: 0px !important;" *ngIf="p.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == 1">\n            <ion-col col-4 *ngIf="p.tax == \'approve\'">\n              <button ion-button small round color="secondary" (tap)="approveFunc(p);"\n                [ngStyle]="{\'background-color\': p.approvebuttonColor}">Approve</button>\n            </ion-col>\n            <ion-col col-6 *ngIf="p.tax == \'reject\'">\n              <button ion-button small round color="secondary" (tap)="approveFunc(p);"\n                [ngStyle]="{\'background-color\': p.approvebuttonColor}">Approve</button>\n            </ion-col>\n            <ion-col col-2 *ngIf="p.tax == \'approve\'">\n              <img src="assets/imgs/right-image-png-4.png" height="20" width="20"\n                style="margin-left: 4px; margin-top: 8px;" />\n            </ion-col>\n\n            <ion-col col-2 style="text-align: right" *ngIf="p.tax == \'reject\'">\n              <img src="assets/imgs/New Project.png" height="20" width="20"\n                style="margin-right: -12px; margin-top: 8px;" />\n            </ion-col>\n\n            <ion-col col-6 style="text-align: right" *ngIf="p.tax == \'approve\'">\n              <button ion-button small round color="danger" (tap)="rejectFunc(p);" *ngIf="p.tax == \'approve\'"\n                [ngStyle]="{\'background-color\': p.rejectbuttonColor}">Reject</button>\n              <button ion-button small round color="danger" (tap)="rejectFunc(p);" *ngIf="p.tax != \'approve\'"\n                [ngStyle]="{\'background-color\': p.rejectbuttonColor}">Reject</button>\n            </ion-col>\n            <ion-col col-4 style="text-align: right" *ngIf="p.tax == \'reject\'">\n              <button ion-button small round color="danger" (tap)="rejectFunc(p);" *ngIf="p.tax == \'approve\'"\n                [ngStyle]="{\'background-color\': p.rejectbuttonColor}">Reject</button>\n              <button ion-button small round color="danger" (tap)="rejectFunc(p);" *ngIf="p.tax != \'approve\'"\n                [ngStyle]="{\'background-color\': p.rejectbuttonColor}">Reject</button>\n            </ion-col>\n\n          </ion-row>\n        </div>\n\n        <ion-row *ngIf="p.showBTN">\n          <ion-col col-10 style="text-align: left">\n            <ion-input class="inst" [(ngModel)]="p.comment" type="text" placeholder="add comments"></ion-input>\n          </ion-col>\n          <ion-col col-2 style="text-align: right">\n            <button ion-button small [disabled]="p.comment == undefined || p.comment == \'\'"\n              (tap)="addComment(p.comment, p, i)">Add</button>\n          </ion-col>\n        </ion-row>\n\n      </div>\n    </ion-list>\n  </div>\n\n  <div class="divNew" *ngIf="typeAR == \'h\'">\n    <ion-list>\n      <div padding *ngFor="let p of obj; let i = index;" class="divClass">\n        <!-- <ion-item *ngFor="let p of obj"> -->\n        <ion-row>\n          <ion-col col-12>\n            <b>Item # {{p.old[0].PurchaseRequisitionRelease[0].LineItemsNew}}</b>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12 ion-text text-wrap>\n            {{p.old[0].PurchaseRequisitionRelease[0].PRR_MATERIAL_DESC}}\n          </ion-col>\n        </ion-row>\n        <!-- <ion-row>\n          <ion-col col-8>\n            <h3 ion-text text-wrap>{{p.old[0].PurchaseRequisitionRelease[0].PRR_MATERIAL_DESC}}</h3>\n          </ion-col>\n          <ion-col col-4 style="text-align: right">\n            <h3>{{p.old[0].PurchaseRequisitionRelease[0].PRR_QUANTITY}}&nbsp;\n              <span\n                *ngIf="!p.old[0].PurchaseRequisitionRelease[0].PR_LINE_UOM[0].$">{{p.old[0].PurchaseRequisitionRelease[0].PR_LINE_UOM}}</span>\n            </h3>\n          </ion-col>\n        </ion-row> -->\n        <!-- <ion-row>\n          <ion-col col-6>\n            <h3>{{p.old[0].PurchaseRequisitionRelease[0].PRR_CURRENCY}}</h3>\n          </ion-col>\n          <ion-col col-6 style="text-align: right">\n            <h1>{{p.old[0].PurchaseRequisitionRelease[0].fracValue}}</h1>\n          </ion-col>\n        </ion-row> -->\n        <ion-row>\n          <ion-col col-4>\n            {{p.old[0].PurchaseRequisitionRelease[0].PRR_QUANTITY}}&nbsp;\n            <span\n              *ngIf="!p.old[0].PurchaseRequisitionRelease[0].PR_LINE_UOM[0].$">{{p.old[0].PurchaseRequisitionRelease[0].PR_LINE_UOM}}</span>\n          </ion-col>\n\n          <ion-col col-8 style="text-align: right">\n            {{p.old[0].PurchaseRequisitionRelease[0].PRR_CURRENCY}}&nbsp;\n            <span style="font-size: 18px;">{{p.old[0].PurchaseRequisitionRelease[0].fracValue}}</span>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12>\n            Requisitioner:&nbsp;\n            <span\n              *ngIf="!p.old[0].PurchaseRequisitionRelease[0].PRR_CREATEDBY[0].$">{{p.old[0].PurchaseRequisitionRelease[0].PRR_CREATEDBY}}</span>\n          </ion-col>\n        </ion-row>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n<ion-footer class="paraHead">\n  <!-- <div *ngIf="lineApp == \'\'"> -->\n  <div *ngIf="typeAR == \'l\'">\n    Sterlite Power\n  </div>\n  <div *ngIf="typeAR == \'l\'">\n    <button ion-button color="light" block (tap)="submitApproval()" *ngIf="lineApp != \'\'">Submit</button>\n  </div>\n  <div *ngIf="typeAR == \'h\'">\n    <ion-row>\n      <ion-col col-6>\n        <button ion-button block color="secondary" (tap)="approvePR()">Approve</button>\n      </ion-col>\n      <ion-col col-6>\n        <button ion-button block color="danger" (tap)="rejectPR()">Reject</button>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/pr-list/pr-details/pr-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__["a" /* HttpSoapProvider */]])
    ], PrDetailsPage);
    return PrDetailsPage;
    var PrDetailsPage_1;
}());

//# sourceMappingURL=pr-details.js.map

/***/ })

});
//# sourceMappingURL=2.js.map