webpackJsonp([6],{

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(362);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ]
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_service_http_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jsencrypt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jsencrypt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TOKEN_KEY = 'session_token';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParam, alertCtrl, url, events, modalCtrl, storage, toastCtrl, httpSoap, file) {
        this.navCtrl = navCtrl;
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.url = url;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.httpSoap = httpSoap;
        this.file = file;
        var uname = localStorage.getItem("userName");
        this.str = (uname).split("#");
        this.user_name = this.str[0];
        this.storage.set(TOKEN_KEY, this.str[2]);
        localStorage.setItem("StartInterval", "StartInterval");
    }
    HomePage_1 = HomePage;
    HomePage.prototype.ngOnInit = function () {
        // debugger
        var that = this;
        // this.url.startLoading().present();
        if (this.navParam.get('pocount_param') != null) {
            this.count = this.navParam.get('pocount_param');
            this.count1 = this.navParam.get('prcount_param');
            if (this.str[1] == "null" || this.str[1] == "N") {
                this.url.startLoadingToast().present();
                this.url.checkSession(function (err, resp) {
                    if (resp) {
                        // that.url.stopLoading();
                        that.gotoFunction();
                    }
                    else {
                        console.log("not getting response becz of err: ", err);
                        that.toastCtrl.create({
                            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                            position: 'bottom',
                            duration: 3000
                        }).present();
                        that.url.stopLoadingToast();
                    }
                });
            }
        }
        else {
            this.url.startLoadingToast().present();
            this.url.checkSession(function (err, resp) {
                if (resp) {
                    // that.url.stopLoading();
                    that.gotoFunction();
                }
                else {
                    console.log("not getting response becz of err: ", err);
                    that.toastCtrl.create({
                        message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                        position: 'bottom',
                        duration: 3000
                    }).present();
                    that.url.stopLoadingToast();
                }
            });
        }
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (localStorage.getItem("StartInterval") != null) {
            this.session_id = setInterval(function () {
                _this.isOKfunction();
            }, 30000);
            localStorage.setItem("intervalID", this.session_id);
        }
    };
    HomePage.prototype.info = function () {
        var alert = this.alertCtrl.create({
            title: "STERLITE POWER",
            message: "This Mobile Application is the property of STERLITE POWER TRANSMISSION LIMITED. It is for authorized use only. Please uninstall the Application if you do not agree to the conditions stated in Acceptable Usage Policy",
            buttons: ['Close']
        });
        alert.present();
    };
    HomePage.prototype.isOKfunction = function () {
        var method = 'IsOk';
        var parameters = [];
        var that = this;
        if (localStorage.getItem("StartInterval") != null) {
            this.url.forExtraSecurity(function (err, resp) {
                if (resp) {
                    localStorage.setItem("newtm", resp);
                    var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
                    var encrUser = encr.getAddition1(localStorage.getItem("userId"));
                    that.userId = '';
                    var uid = encrUser;
                    that.storage.get(TOKEN_KEY).then(function (res) {
                        if (res) {
                            that.checkisOK(parameters, method, that, uid, res);
                        }
                    });
                }
                else {
                    that.url.stopLoadingToast();
                }
            });
        }
    };
    HomePage.isOkParams = function (userId, sessionid) {
        var parameters = [];
        parameters["User_Id"] = userId;
        parameters["session_id"] = sessionid;
        return parameters;
    };
    HomePage.prototype.checkisOK = function (parameters, method, that, uid, sessionid) {
        parameters['IsOk xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HomePage_1.isOkParams(uid, sessionid);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                var trip = response;
                var s = trip['SOAP:Envelope'];
                if (s['SOAP:Body'][0]['SOAP:Fault']) {
                    that.callLogout();
                    var toast = that.toastCtrl.create({
                        message: "Session expired",
                        duration: 2000,
                        position: "top"
                    });
                    toast.present();
                    that.url.stopLoading();
                }
            }
            else {
                that.url.stopLoading();
            }
        });
    };
    HomePage.prototype.changePass = function () {
        var profileModal = this.modalCtrl.create('ChangePasswordPage', {
            param: 'byuser'
        });
        profileModal.present();
    };
    HomePage.prototype.doRefresh = function (refresher) {
        console.log("On refresh: ", Math.floor(new Date().getTime() / 1000.0));
        var that = this;
        this.url.startLoadingToast().present();
        this.url.checkSession(function (err, resp) {
            if (resp) {
                // that.url.stopLoading();
                that.tk = undefined;
                that.tk = localStorage.getItem("gfdsa");
                localStorage.setItem("newtm", resp);
                that.getPOcount();
            }
            else {
                console.log("not getting response becz of err: ", err);
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoadingToast();
            }
        });
        setTimeout(function () {
            refresher.complete();
        }, 100);
    };
    HomePage.prototype.getPOcount = function () {
        var method = 'GetPendingCounts';
        var parameters = [];
        var that = this;
        // localStorage.setItem("newtm", resp)
        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
        var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        that.userId = '';
        that.userId = encrUser;
        that.getPOsubCode(parameters, method, that);
    };
    HomePage.userPOcount = function (username, tk) {
        var parameters = [];
        parameters["userId"] = username;
        parameters["tk"] = tk;
        return parameters;
    };
    HomePage.prototype.envelopeBuilder = function (requestBody) {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    };
    HomePage.prototype.polist = function () {
        // debugger
        var that = this;
        console.log(this.url.loadingToast);
        if (this.url.loadingToast) {
            this.url.stopLoadingToast();
        }
        // that.url.stopLoadingToast();
        this.navCtrl.push('PoListPage', {
            param: this.userId,
            count: that.count
        });
    };
    HomePage.prototype.prlist = function () {
        // debugger
        var that = this;
        if (this.url.loadingToast) {
            this.url.stopLoadingToast();
        }
        // that.url.stopLoadingToast();
        this.navCtrl.push('PRListPage', {
            param: this.userId,
            count: that.count1
        });
    };
    HomePage.prototype.callLogout = function () {
        var method = 'LogoutAuthUser';
        var parameters = [];
        var that = this;
        var encr = new __WEBPACK_IMPORTED_MODULE_4_jsencrypt__["JSEncrypt"]();
        var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        var uId = encrUser;
        that.logoutFunc(parameters, method, that, uId);
    };
    HomePage.logoutParams = function (username) {
        var parameters = [];
        parameters["USER_ID"] = username;
        return parameters;
    };
    HomePage.prototype.logoutFunc = function (parameters, method, that, uId) {
        parameters['LogoutAuthUser xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HomePage_1.logoutParams(uId);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        that.navCtrl.setRoot("LoginPage");
        this.httpSoap.testService(parameters, function (err, response) {
            ///////// delete all temp(cache) files from device //////
            // debugger
            // var directoryEntry = that.file.externalApplicationStorageDirectory;
            // that.file.getFile(directoryEntry, 'statement.pdf', flags)
            // that.file.removeFile(path, fileName);
            // that.file.listDir(that.file.cacheDirectory, 'io.smartapprovalsiOS.com').then((result) => {
            //   console.log('files in io.smartapprovalsiOS.com directory: ', JSON.stringify(result));
            //   console.log('Started deleting files from cache folder!');
            //   for (let file of result) {
            //     if (file.isFile == true) {
            //       that.file.removeFile(that.file.cacheDirectory + '/io.smartapprovalsiOS.com/', file.name).then(data => {
            //         console.log('file removed: ', that.file);
            //         data.fileRemoved.getMetadata(function (metadata) {
            //           let name = data.fileRemoved.name;
            //           let size = metadata.size;
            //           let fullPath = data.fileRemoved.fullPath;
            //           console.log('Deleted file: ', name, size, fullPath);
            //           console.log('Name: ' + name + ' / Size: ' + size);
            //         });
            //       }).catch(error => {
            //         file.getMetadata(function (metadata) {
            //           let name = file.name;
            //           let size = metadata.size;
            //           console.log('Error deleting file from cache folder: ', error);
            //           console.log('Name: ' + name + ' / Size: ' + size);
            //         });
            //       });
            //     }
            //   }
            // });
            //////// end file cleaning /////
            if (response) {
                var inID = JSON.parse(localStorage.getItem("intervalID"));
                clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("LOGGEDIN");
                localStorage.removeItem("oldPass");
                localStorage.removeItem("userId");
                localStorage.removeItem("userName");
                localStorage.removeItem("userdetail");
                localStorage.removeItem("trewq");
                localStorage.removeItem("gfdsa");
                localStorage.removeItem("newtm");
                localStorage.removeItem("StartInterval");
                that.storage.remove("session_token");
                // that.navCtrl.setRoot("LoginPage");
            }
            else {
                console.log("not getting response becz of err: ", err);
                var inID = JSON.parse(localStorage.getItem("intervalID"));
                clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("LOGGEDIN");
                localStorage.removeItem("oldPass");
                localStorage.removeItem("userId");
                localStorage.removeItem("userName");
                localStorage.removeItem("userdetail");
                localStorage.removeItem("trewq");
                localStorage.removeItem("gfdsa");
                localStorage.removeItem("newtm");
                localStorage.removeItem("StartInterval");
                that.storage.remove("session_token");
                that.url.stopLoading();
            }
        });
    };
    HomePage.prototype.logout = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Do you want to exit Smart Approvals?",
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.callLogout();
                    }
                },
                {
                    text: "Back"
                }]
        });
        alert.present();
    };
    HomePage.prototype.gotoFunction = function () {
        if (this.str[1] == "null" || this.str[1] == "N") {
            this.tk = localStorage.getItem("gfdsa");
            if (localStorage.getItem("PasswordChnaged") == null) {
                this.url.stopLoadingToast();
                var profileModal = this.modalCtrl.create('ChangePasswordPage');
                profileModal.present();
            }
            else {
                if (localStorage.getItem("PasswordChnaged") == "PasswordChnaged") {
                    this.userId = localStorage.getItem("userId");
                    this.getPOcount();
                }
            }
        }
        else {
            if (this.str[1] == "Y") {
                this.tk = localStorage.getItem("gfdsa");
                this.userId = localStorage.getItem("userId");
                this.getPOcount();
            }
        }
    };
    HomePage.prototype.getPOsubCode = function (parameters, method, that) {
        parameters['GetPendingCounts xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HomePage_1.userPOcount(that.userId, that.tk);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // that.url.startLoading().present();
        console.log("before GetPendingCounts: ", Math.floor(new Date().getTime() / 1000.0));
        this.httpSoap.testService(parameters, function (err, response) {
            console.log("after GetPendingCounts: ", Math.floor(new Date().getTime() / 1000.0));
            if (response) {
                that.url.stopLoadingToast();
                // debugger;
                var trip = response;
                var s = trip['SOAP:Envelope'];
                if (s['SOAP:Body'] == undefined) {
                    // that.url.startLoading();
                    var alert_1 = that.alertCtrl.create({
                        message: 'Something went wrong. Please visit after some time.',
                        buttons: [{
                                text: 'Okay'
                            }]
                    });
                    alert_1.present();
                }
                else {
                    try {
                        console.log("before render: ", Math.floor(new Date().getTime() / 1000.0));
                        var a = s['SOAP:Body'][0].GetPendingCountsResponse;
                        var b = a[0].tuple[0].old[0].getPendingCounts[0].getPendingCounts[0];
                        var split1 = b.split("#");
                        var po = split1[0].split(":");
                        that.count = po[1];
                        var pr = split1[1].split(":");
                        that.count1 = pr[1];
                        console.log("after render: ", Math.floor(new Date().getTime() / 1000.0));
                        // that.count = b;
                        // that.getPRcount();
                    }
                    catch (e) {
                        var a = s['SOAP:Body'][0]['SOAP:Fault'];
                        var b = a[0].faultstring[0]._;
                        console.log("catch token expired: ", b);
                        if (b == 'EXPIRED_SESSION') {
                            that.callLogout();
                        }
                        else {
                            var toast = that.toastCtrl.create({
                                message: "Token expired!! Please refresh once..",
                                duration: 2500,
                                position: "top"
                            });
                            toast.present();
                        }
                    }
                }
            }
            else {
                that.url.stopLoadingToast();
            }
        });
    };
    HomePage = HomePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="sterlite">\n    <ion-title>\n      <div style="text-align: center;">\n        <img src="assets/imgs/sterlogo.png" height="13px" />\n      </div>\n      <div style="text-align: center; font-size: 12px; letter-spacing: 2px;">Welcome, {{user_name}}</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only clear (click)="info()">\n        <ion-icon name="information-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="mainContent">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-row  style="margin-top: 15%;padding: 8px;">\n  <div style="width: 100%; float: left; ">\n    <ion-col class="buttonClass" (tap)="prlist()">\n      <p style="font-size: 18px;">Purchase Requisitions</p>\n      <span class="spanSt">{{count1}}</span>\n    </ion-col>\n  </div>\n  </ion-row>\n\n  <ion-row style="padding: 8px; ">\n    \n    <div style="width:100%; float: right;">\n      <ion-col class="buttonClass" (tap)="polist()">\n        <p style="font-size: 18px;">Purchase Orders</p>\n        <span class="spanSt">{{count}}</span>\n      </ion-col>\n    </div>\n  </ion-row>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar color="sterlite">\n    <ion-row>\n      <ion-col col-6 style="letter-spacing: 1px; text-align: center; color: #fff; border-right: 1px solid #fff;" (tap)="changePass()">\n        <ion-icon name="person"></ion-icon>&nbsp;Change Password\n      </ion-col>\n      <ion-col col-6 style="letter-spacing: 1px; text-align: center; color: #fff; border-left: 1px solid #fff;" (tap)="logout()">\n        <ion-icon name="log-out"></ion-icon>&nbsp;Logout\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/ionic-smartapprovals/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_http_service_http_service__["a" /* HttpServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_http_soap_http_soap__["a" /* HttpSoapProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */]])
    ], HomePage);
    return HomePage;
    var HomePage_1;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=6.js.map