import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpServiceProvider } from '../../providers/http-service/http-service';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../../providers/http-soap/http-soap';

@IonicPage()
@Component({
  selector: 'page-ot-ppage',
  templateUrl: 'ot-ppage.html',
})
export class OtPpagePage {
  private OTPForm: FormGroup;
  usermail: string;
  signinObj: any;
  username: any;
  otPpass: any;
  validity: string;
  isUnchanged: boolean = false;

  constructor(
    public url: HttpServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public events: Events,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public httpSoap: HttpSoapProvider
  ) {

    this.signinObj = localStorage.getItem('userdetail');

    this.OTPForm = this.formBuilder.group({
      otp1: ["", Validators.required],
      otp2: ["", Validators.required],
      otp3: ["", Validators.required],
      otp4: ["", Validators.required]
    })
  }

  ionViewDidLoad() { }

  login2() {
    this.navCtrl.setRoot("LoginPage");
  }

  getCodeBoxElement(index) {
    let inputValue = (document.getElementById('codeBox' + index) as HTMLInputElement);
    return inputValue;
  }
  onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if (this.getCodeBoxElement(index).value.length === 1) {
      if (index !== 4) {
        this.getCodeBoxElement(index + 1).focus();
      } else {
        this.getCodeBoxElement(index).blur();
      }
    }
    if (eventCode === 8 && index !== 1) {
      this.getCodeBoxElement(index - 1).focus();
    }
  }
  onFocusEvent(index) {
    for (var item = 1; item < index; item++) {
      const currentElement = this.getCodeBoxElement(item);
      if (!currentElement.value) {
        currentElement.focus();
        break;
      }
    }
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }


  doSignUpOtPS() {

    if (this.signinObj) {
      this.isUnchanged = true;
      var str = this.signinObj.split('@');
      var method: string = 'AuthenticateStlUserOTP';
      var parameters: {}[] = [];
      this.username = str[0];
      this.otPpass = this.OTPForm.value.otp1.toString() + this.OTPForm.value.otp2.toString() + this.OTPForm.value.otp3.toString() + this.OTPForm.value.otp4.toString();
      this.validity = "30";
      let that = this;
      this.url.startLoading().present();
      this.url.forExtraSecurity(function (err, resp) {
        if (resp) {
          localStorage.setItem("newtm", resp)
          var encr = new Encrypt.JSEncrypt();
          var userNa = encr.getAddition1(that.username);
          that.getSubCode(parameters, method, that, userNa);
        } else {
          console.log("not getting response becz of err: ", err)
          that.toastCtrl.create({
            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
            position: 'bottom',
            duration: 3000
          }).present();
          that.url.stopLoading();
          that.isUnchanged = false;
        }
      });
    }
  }

  private static userSignupgen(username, password): {}[] {
    var parameters: {}[] = [];
    parameters["EMAIL_ID"] = username;
    parameters["PWD_LENGTH"] = password;
    return parameters;
  }

  private static userOTP(username, otPpass, validity): {}[] {
    var parameters: {}[] = [];
    parameters["userId"] = username;
    parameters["OTP"] = otPpass;
    parameters["Validity"] = validity;
    return parameters;
  }

  getSubCode(parameters, method, that, userNa) {
    parameters['AuthenticateStlUserOTP   xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata"'] = OtPpagePage.userOTP(userNa, that.otPpass, that.validity);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    // that.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      if (response) {
        // that.url.stopLoading();
        var s = response['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].AuthenticateStlUserOTPResponse;
        var b = a[0].tuple[0].old[0].authenticateStlUserOTP[0].authenticateStlUserOTP[0]
        var signObj = that.signinObj;
        if (b == 'VALID') {
          if (signObj) {
            var str = signObj.split('@');
            var aft = str[1];
            var method: string = 'CreateSTLAuthUser';
            var parameters: {}[] = [];

            that.url.forExtraSecurity(function (err, resp) {
              if (resp) {
                localStorage.setItem("newtm", resp)
                var encr = new Encrypt.JSEncrypt();
                var username = encr.getAddition1((signObj).toString().toLowerCase());;
                var passwordLength = "6";
                that.getSubCodeSubCode(parameters, method, that, username, passwordLength);
              } else {
                console.log("not getting response becz of err: ", err)
                that.toastCtrl.create({
                  message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                  position: 'bottom',
                  duration: 3000
                }).present();
                that.url.stopLoading();
              }
            });
          } else {
            let alert = this.alertCtrl.create({
              message: b,
              buttons: [{
                text: 'Okay',
                handler: () => {
                  that.OTPForm.reset();
                }
              }]
            });
            alert.present();
          }
        } else if (b == "INVALID/EXPIRED OTP") {
          that.url.stopLoading();
          that.isUnchanged = false;
          let alert = that.alertCtrl.create({
            message: b,
            buttons: [{
              text: 'Okay',
              handler: () => {
                that.OTPForm.reset();
              }
            }]
          });
          alert.present();
        }
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
        that.isUnchanged = false;
      }
    })
  }

  getSubCodeSubCode(parameters, method, that, username, passwordlength) {
    parameters['CreateSTLAuthUser xmlns="http://schemas.cordys.com/default"'] = OtPpagePage.userSignupgen(username, passwordlength);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    // that.url.startLoading().present();
    this.httpSoap.testService(parameters, function (err, response) {
      if (response) {
        that.url.stopLoading();
        let toast = that.toastCtrl.create({
          message: 'Your new password has been sent on your e-mail.',
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          that.navCtrl.setRoot('LoginPage');
        });

        toast.present();
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoading();
      }
    })
  }
}
