import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PoListPage } from './po-list';

@NgModule({
  declarations: [
    PoListPage,
  ],
  imports: [
    IonicPageModule.forChild(PoListPage),
  ],
})
export class PoListPageModule {}
