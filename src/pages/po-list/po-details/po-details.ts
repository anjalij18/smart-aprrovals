import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, LoadingController, AlertController, ViewController } from 'ionic-angular';
import { PopoverPO } from './popover-po/popover-po';
import { HttpServiceProvider } from '../../../providers/http-service/http-service';
import { ToastController } from 'ionic-angular';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../../../providers/http-soap/http-soap';

@IonicPage()
@Component({
  selector: 'page-po-details',
  templateUrl: 'po-details.html',
})
export class PoDetailsPage implements OnInit {
  poData: any;
  objhead: any = [];
  objline: any = [];
  task_id: any;
  pdflink: string;
  loading: any;
  tk: any;
  docsList: any[] = [];
  popover: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public httpSer: HttpServiceProvider,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public httpSoap: HttpSoapProvider,
    public viewCtrl: ViewController
  ) {
    var dTa = navParams.get('param');
    this.poData = dTa.old[0].STL_TRA_PORELEASE[0];
    this.task_id = this.poData.por_temp3;
  }

  ionViewDidLoad() { }

  ngOnInit() {
    let that = this;
    this.httpSer.startLoading().present();
    this.httpSer.checkSession(function (err, resp) {
      if (resp) {
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp);
        that.getPOlistDetails();
      } else {
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.httpSer.stopLoadingToast();
      }
    });
    this.getDocsList();
  }

  goNext() {
    let that = this;
    this.navCtrl.push("LineLevelPage", {
      param: that.objline,
      name: that.poData.por_ponumber[0],
      taskId: that.task_id
    })
  }

  showPopover(ev, data) {
    console.log("what is in data: ", data);
    this.popover = this.popoverCtrl.create(PopoverPO,
      {
        poData: data
      },
      {
        cssClass: 'contact-popover'
      });

    this.popover.onDidDismiss(() => { })

    this.popover.present({
      ev: ev
    });
  }

  getPOlistDetails() {
    var method: string = 'PO_Mail_Details';
    var parameters: {}[] = [];
    let that = this;
    var encr = new Encrypt.JSEncrypt();
    var enrPOnumber = encr.getAddition1(that.poData.por_ponumber);
    var updatedPOnumber = enrPOnumber;
    that.getPOlistDetailssubCode(parameters, method, that, updatedPOnumber);
  }

  private static userPODetails(po_num, tk): {}[] {
    var parameters: {}[] = [];
    parameters["PONumber"] = po_num;
    parameters["tk"] = tk;
    return parameters;
  }

  private static docsPODetails(po_num, tk): {}[] {
    var parameters: {}[] = [];
    parameters["poNumber"] = po_num;
    parameters["tk"] = tk;
    return parameters;
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }

  approvePO() {
    let alert = this.alertCtrl.create({
      message: "Are you sure you want to proceed?",
      buttons: [{
        text: 'PROCEED',
        handler: () => {
          debugger;
          const HTTPnew = new XMLHttpRequest();
          const httpurl = "https://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=" + this.task_id + "&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve";
          // const httpurl = 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + this.task_id + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve';
          HTTPnew.open("GET", httpurl);
          HTTPnew.send();
          let toast = this.toastCtrl.create({
            message: 'Your request has been processed sucessfully!',
            duration: 3000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            this.navCtrl.setRoot('HomePage');
          });

          toast.present();
        },
        cssClass: 'btton'
      },
      {
        text: 'NO'
      }],
      cssClass: 'alertCustomCss' // <- added this
    });
    alert.present();

  }

  rejectPO() {
    let alert = this.alertCtrl.create({
      message: "Are you sure you want to proceed?",
      buttons: [{
        text: 'PROCEED',
        handler: () => {
          const HTTPnew = new XMLHttpRequest();
          const httpurl = "http://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=" + this.task_id + "&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject";
          // const httpurl = 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + this.task_id + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject';
          HTTPnew.open("GET", httpurl);
          HTTPnew.send();
          let toast = this.toastCtrl.create({
            message: 'Your request has been processed sucessfully!',
            duration: 3000,
            position: 'bottom'
          });
          toast.onDidDismiss(() => {
            this.navCtrl.setRoot('HomePage');
          });
          toast.present();

        },
        cssClass: 'btton'
      },
      {
        text: 'NO'
      }],
      cssClass: 'alertCustomCss' // <- added this
    });
    alert.present();

  }

  getPOlistDetailssubCode(parameters, method, that, updatedPOnumber) {
    parameters['PO_Mail_Details xmlns="http://schemas.cordys.com/default"'] = PoDetailsPage.userPODetails(updatedPOnumber, that.tk);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    this.httpSoap.testService(parameters, function (err, response) {

      if (response) {
        that.httpSer.stopLoading();
        var temp = response;
        var s = temp['SOAP:Envelope'];
        try {
          var a = s['SOAP:Body'][0].PO_Mail_DetailsResponse;
          var head = a[0].PODetails[0].HeaderDetails[0];
          var line = a[0].PODetails[0].LineDetails[0].Line;
          that.objhead = head;
          var formatter = new Intl.NumberFormat('en-US', {
            minimumFractionDigits: 2,
          });

          that.objhead.netvalnum = formatter.format(that.objhead.NetValue); /* $2,500.00 */
          that.objhead.CreatedOnNew = new Date(that.objhead.CreatedOn).toISOString();
          that.objline = line;
          for (var i = 0; i < that.objline.length; i++) {
            var str = that.objline[i].LineItems.toString();
            var r = str.replace(/^0+|0+$/, "");
            that.objline[i].LineItemsNew = r;
            that.objline[i].showDiv = false;
            that.objline[i].netvalnum = formatter.format(that.objline[i].NetPrice); /* $2,500.00 */
          }
        }
        catch (e) {
          var a = s['SOAP:Body'][0]['SOAP:Fault'];
          var b = a[0].faultstring[0]._;
          let toast = that.toastCtrl.create({
            message: "Token expired!! Please refresh once..",
            duration: 2500,
            position: 'top'
          });
          toast.present();
        }
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.httpSer.stopLoading()
      }
    })
  }

  getDocsList() {
    let that = this;
    that.tk = undefined;
    this.httpSer.checkSession(function (err, resp) {
      if (resp) {
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp);
        that.getDocsListFunc();
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
      }
    });
  }

  getDocsListFunc() {
    var parameters: {}[] = [];
    let that = this;
    var encr = new Encrypt.JSEncrypt();
    var enrPOnumber = encr.getAddition1(that.poData.por_ponumber);
    var encryptedPOnum = enrPOnumber;
    that.getDocs(encryptedPOnum, parameters)
  }

  getDocs(encryptedPOnum, parameters) {
    let that = this;
    // parameters['GetPOAvailableAttachments xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PoDetailsPage.docsPODetails(encryptedPOnum, that.tk);
    // parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    parameters['GetPOAvailableAttachmentsFromSAP xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = PoDetailsPage.docsPODetails(encryptedPOnum, that.tk);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    this.httpSoap.testService(parameters, function (err, response) {
      debugger
      if (response) {
        var temp = response;
        var s = temp['SOAP:Envelope'];
        try {
          // var a = s['SOAP:Body'][0].GetPOAvailableAttachmentsResponse;
          // var listData = a[0].tuple;
          // that.docsList = listData;
          var a = s['SOAP:Body'][0].GetPOAvailableAttachmentsFromSAPResponse;
          var b = a[0].ZMM_PO_ATTACHMENT_NAMESResponse;
          var listData = b[0].ET_ATTACHMENT_NAMES;
          that.docsList = listData;

          console.log("DOCID : ", that.docsList[0].item[0].DOCID)
          console.log("FILE_TYPE : ", that.docsList[0].item[0].FILE_TYPE)
          console.log("FILENAME : ", that.docsList[0].item[0].FILENAME)

          // console.log("PONUMBER : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].PONUMBER)
          // console.log("ATTACH_ID : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].ATTACH_ID)
          // console.log("FILESIZE : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].FILESIZE)
          // console.log("FILENAME : ", that.docsList[0].old[0].STL_PO_ATTACHMENTS[0].FILENAME)

        }
        catch (e) {
          var a = s['SOAP:Body'][0]['SOAP:Fault'];
          var b = a[0].faultstring[0]._;
          console.log("token err: ", JSON.stringify(b));
          let toast = that.toastCtrl.create({
            message: "Token expired!! Please refresh once..",
            duration: 2500,
            position: 'top'
          });
          toast.present();
        }
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();

      }
    })
  }

  openFileList(ev) {
    let that = this;
    console.log("view doc list: ", that.docsList)
    let popover = this.popoverCtrl.create(PopoverPO,
      {
        poData: that.docsList,
        poNumber: that.poData.por_ponumber
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => { })

    popover.present({
      ev: ev
    });
  }

}
