import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { IonPullupModule } from 'ionic-pullup';
// import { SoapService } from '../../providers/soap-service/soap-service';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    IonPullupModule
  ],
  
   providers: [
    //  {provide: String, useValue: "SoapService"}
    // SoapService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class LoginPageModule {}
