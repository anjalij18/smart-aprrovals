import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonPullupModule } from 'ionic-pullup';
import { PRListPage } from './pr-list';

@NgModule({
  declarations: [
    PRListPage,
  ],
  imports: [
    IonicPageModule.forChild(PRListPage),
    IonPullupModule
  ],
})
export class PRListPageModule {}