import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrDetailsPage } from './pr-details';
// import { PopoverPR } from './popover-pr/popover-pr';

@NgModule({
  declarations: [
    PrDetailsPage,
    // PopoverPR
  ],
  imports: [
    IonicPageModule.forChild(PrDetailsPage),
  ],
  entryComponents: [
    // PopoverPR
  ]
})
export class PrDetailsPageModule {}
