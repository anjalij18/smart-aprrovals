import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangePasswordPage } from './change-password';
// import { SoapService } from '../../../providers/soap-service/soap-service';

@NgModule({
  declarations: [
    ChangePasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangePasswordPage),
  ],
  providers: [
    //  {provide: String, useValue: "SoapService"}
    // SoapService
  ],
})
export class ChangePasswordPageModule {}