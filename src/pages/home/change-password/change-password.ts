import { Component } from "@angular/core";
import { IonicPage, NavParams, ViewController, ToastController, NavController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpServiceProvider } from "../../../providers/http-service/http-service";
import * as Encrypt from 'jsencrypt';
import { Storage } from "@ionic/storage";
import { HttpSoapProvider } from "../../../providers/http-soap/http-soap";
const TOKEN_KEY = 'session_token';

@IonicPage()
@Component({
    selector: 'page-change-password',
    templateUrl: './change-password.html'
})

export class ChangePasswordPage {
    private changePassForm: FormGroup;
    userId: string;
    showForm: boolean = false;
    isUnchanged: boolean = false;
    session_id: any;

    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        private fb: FormBuilder,
        public toastCtrl: ToastController,
        public url: HttpServiceProvider,
        public navCtrl: NavController,
        public storage: Storage,
        public httpSoap: HttpSoapProvider
    ) {
        this.userId = localStorage.getItem("userId");

        if (params.get('param') != null) {
            this.showForm = true;
            this.changePassForm = this.fb.group({
                oldPass: ['', Validators.required],
                newPass: ['', Validators.required],
                cnewPass: ['', Validators.required]
            });
        } else {
            this.showForm = false;
            this.changePassForm = this.fb.group({
                oldPass: [localStorage.getItem("oldPass"), Validators.required],
                newPass: ['', Validators.required],
                cnewPass: ['', Validators.required]
            });
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    isOKfunction() {
        var method: string = 'IsOk';
        var parameters: {}[] = [];
        let that = this;
        if (localStorage.getItem("StartInterval") != null) {
            this.url.forExtraSecurity(function (err, resp) {
                if (resp) {
                    localStorage.setItem("newtm", resp)
                    var encr = new Encrypt.JSEncrypt();
                    var encrUser = encr.getAddition1(localStorage.getItem("userId"));
                    that.userId = '';
                    var uid = encrUser;
                    that.storage.get(TOKEN_KEY).then((res) => {
                        if (res) {
                            that.checkisOK(parameters, method, that, uid, res);
                        }
                    })
                }
            });
        }
    }

    private static isOkParams(userId, sessionid): {}[] {
        var parameters: {}[] = [];
        parameters["User_Id"] = userId;
        parameters["session_id"] = sessionid;
        return parameters;
    }

    checkisOK(parameters, method, that, uid, sessionid) {
        parameters['IsOk xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = ChangePasswordPage.isOkParams(uid, sessionid);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        this.httpSoap.testService(parameters, function (err, response) {
            if (response) {
                var trip = response;
                var s = trip['SOAP:Envelope'];
                if (s['SOAP:Body'][0]['SOAP:Fault']) {
                    that.callLogout();
                    let toast = that.toastCtrl.create({
                        message: "Session expired",
                        duration: 2000,
                        position: "top"
                    });
                    toast.present();
                }
            }
        })
    }

    submit() {
        this.isUnchanged = true;
        debugger;
        if (this.changePassForm.value.oldPass == this.changePassForm.value.newPass) {
            const toast = this.toastCtrl.create({
                message: 'Old password and New password can not be same!! Please try again..',
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(() => {
                this.changePassForm.patchValue({
                    newPass: '',
                    cnewPass: ''
                });
                this.isUnchanged = false;
            });
            toast.present();
        } else {
            if (this.changePassForm.value.newPass != this.changePassForm.value.cnewPass) {
                const toast = this.toastCtrl.create({
                    message: 'New password and confirm password should matched!! Please try again..',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.onDidDismiss(() => {
                    this.changePassForm.patchValue({
                        newPass: '',
                        cnewPass: ''
                    });
                    this.isUnchanged = false;
                });
                toast.present();
            } else {
                var inID = JSON.parse(localStorage.getItem("intervalID"));
                clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("StartInterval");
                var method: string = 'ChangeSTLUserPassword';
                var parameters: {}[] = [];
                let that = this;
                this.url.startLoading().present();
                this.url.checkSession(function (err, resp) {
                    if (resp) {
                        localStorage.setItem("newtm", resp)
                        var encr = new Encrypt.JSEncrypt();
                        var uid = encr.getAddition1(localStorage.getItem("userId"));
                        var oldpass = encr.getAddition2(that.changePassForm.value.oldPass);
                        var newPass = encr.getAddition2(that.changePassForm.value.newPass);
                        that.updatePassFunc(parameters, method, that, uid, oldpass, newPass);
                    } else {
                        console.log("not getting response becz of err: ", err)
                        that.toastCtrl.create({
                            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                            position: 'bottom',
                            duration: 3000
                        }).present();
                        that.url.stopLoading();
                        that.isUnchanged = false;
                    }
                });

            }
        }
    }

    private static changePass(uid, opass, newpass): {}[] {
        var parameters: {}[] = [];

        parameters["User_ID"] = uid;
        parameters["Old_Password"] = opass;
        parameters["New_Password"] = newpass;

        return parameters;
    }

    envelopeBuilder(requestBody: string): string {
        return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<SOAP:Body>" +
            requestBody +
            "</SOAP:Body>" +
            "</SOAP:Envelope>";
    }

    updatePassFunc(parameters, method, that, uid, oldpass, newPass) {
        parameters['ChangeSTLUserPassword xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = ChangePasswordPage.changePass(uid, oldpass, newPass);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

        // this.url.startLoading().present();
        this.httpSoap.testService(parameters, function (err, response) {

            if (response) {
                var s = response['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].ChangeSTLUserPasswordResponse;
                var b = a[0].tuple[0].old[0].changeSTLUserPassword[0].changeSTLUserPassword[0]
                if (b == "Password changed sucessfully") {

                    let toast = that.toastCtrl.create({
                        message: 'You have sucessfully changed the password. Please login with your new password.',
                        duration: 5000,
                        position: 'bottom'
                    });
                    toast.onDidDismiss(() => {

                        that.callLogout();
                    });

                    toast.present();
                } else {
                    that.isUnchanged = false;
                    localStorage.setItem("StartInterval", "StartInterval");
                    if (localStorage.getItem("StartInterval") != null) {
                        that.session_id = setInterval(() => {
                            that.isOKfunction();
                        }, 30000)

                        localStorage.setItem("intervalID", that.session_id);
                    }
                    that.url.stopLoading();
                    let toast = that.toastCtrl.create({
                        message: 'Old password is wrong, Please try with valid password..',
                        duration: 2500,
                        position: 'bottom'
                    });

                    toast.onDidDismiss(() => {
                        that.changePassForm.reset();
                    });
                    toast.present();
                }
            } else {
                that.isUnchanged = false;
                console.log("not getting response becz of err: ", err)
                that.toastCtrl.create({
                    message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                    position: 'bottom',
                    duration: 3000
                }).present();
                that.url.stopLoading();
            }
        })
    }

    callLogout() {
        var method: string = 'LogoutAuthUser';
        var parameters: {}[] = [];
        let that = this;
        // this.url.checkSession(function (err, resp) {
        //     if (resp) {
        //         localStorage.setItem("newtm", resp)
        var encr = new Encrypt.JSEncrypt();
        var encrUser = encr.getAddition1(localStorage.getItem("userId"));
        var uId = encrUser;

        that.logoutFunc(parameters, method, that, uId);
    }

    private static logoutParams(username): {}[] {
        var parameters: {}[] = [];
        parameters["USER_ID"] = username;
        return parameters;
    }

    logoutFunc(parameters, method, that, uId) {
        parameters['LogoutAuthUser xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = ChangePasswordPage.logoutParams(uId);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        that.url.stopLoading();
        var inID = JSON.parse(localStorage.getItem("intervalID"));
        clearInterval(inID);
        that.navCtrl.setRoot("LoginPage");
        this.httpSoap.testService(parameters, function (err, response) {

            if (response) {
                // that.url.stopLoading();
                that.viewCtrl.dismiss();

                // var inID = JSON.parse(localStorage.getItem("intervalID"));
                // clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("LOGGEDIN");
                localStorage.removeItem("oldPass");
                localStorage.removeItem("userId");
                localStorage.removeItem("userName");
                localStorage.removeItem("userdetail");
                localStorage.removeItem("trewq");
                localStorage.removeItem("gfdsa");
                localStorage.removeItem("newtm");
                localStorage.removeItem("StartInterval");
                that.storage.remove("session_token");
                // that.navCtrl.setRoot("LoginPage");
            } else {
                // console.log("not getting response becz of err: ", err)
                // that.toastCtrl.create({
                //     message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                //     position: 'bottom',
                //     duration: 3000
                // }).present();
                // that.url.stopLoading();
                that.viewCtrl.dismiss();

                // var inID = JSON.parse(localStorage.getItem("intervalID"));
                // clearInterval(inID);
                localStorage.removeItem("intervalID");
                localStorage.removeItem("LOGGEDIN");
                localStorage.removeItem("oldPass");
                localStorage.removeItem("userId");
                localStorage.removeItem("userName");
                localStorage.removeItem("userdetail");
                localStorage.removeItem("trewq");
                localStorage.removeItem("gfdsa");
                localStorage.removeItem("newtm");
                localStorage.removeItem("StartInterval");
                that.storage.remove("session_token");
            }
        })
    }
}