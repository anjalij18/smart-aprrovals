import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, ModalController, ToastController } from 'ionic-angular';
import { HttpServiceProvider } from '../../providers/http-service/http-service';
import { Storage } from '@ionic/storage';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../../providers/http-soap/http-soap';
import { File } from '@ionic-native/file';
const TOKEN_KEY = 'session_token';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  user: string;
  userId: string;
  count: any;
  count1: any;
  user_name: string;
  timestamp: any;
  str: any;
  tk: any;
  session_id: any;

  constructor(
    public navCtrl: NavController,
    public navParam: NavParams,
    public alertCtrl: AlertController,
    public url: HttpServiceProvider,
    public events: Events,
    public modalCtrl: ModalController,
    private storage: Storage,
    public toastCtrl: ToastController,
    public httpSoap: HttpSoapProvider,
    public file: File
  ) {

    var uname = localStorage.getItem("userName");
    this.str = (uname).split("#");
    this.user_name = this.str[0];
    this.storage.set(TOKEN_KEY, this.str[2]);
    localStorage.setItem("StartInterval", "StartInterval");
  }

  ngOnInit() {
    // debugger
    let that = this;
    // this.url.startLoading().present();
    if (this.navParam.get('pocount_param') != null) {
      this.count = this.navParam.get('pocount_param');
      this.count1 = this.navParam.get('prcount_param');
      if (this.str[1] == "null" || this.str[1] == "N") {
        this.url.startLoadingToast().present();
        this.url.checkSession(function (err, resp) {
          if (resp) {
            // that.url.stopLoading();
            that.gotoFunction();
          } else {
            console.log("not getting response becz of err: ", err)
            that.toastCtrl.create({
              message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
              position: 'bottom',
              duration: 3000
            }).present();
            that.url.stopLoadingToast();
          }
        });
      }
    } else {
      this.url.startLoadingToast().present();
      this.url.checkSession(function (err, resp) {
        if (resp) {
          // that.url.stopLoading();
          that.gotoFunction();
        } else {
          console.log("not getting response becz of err: ", err)
          that.toastCtrl.create({
            message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
            position: 'bottom',
            duration: 3000
          }).present();
          that.url.stopLoadingToast();
        }
      });
    }
  }

  ionViewDidEnter() {
    if (localStorage.getItem("StartInterval") != null) {
      this.session_id = setInterval(() => {
        this.isOKfunction();
      }, 30000)

      localStorage.setItem("intervalID", this.session_id);
    }
  }

  info() {
    let alert = this.alertCtrl.create({
      title: "STERLITE POWER",
      message: "This Mobile Application is the property of STERLITE POWER TRANSMISSION LIMITED. It is for authorized use only. Please uninstall the Application if you do not agree to the conditions stated in Acceptable Usage Policy",
      buttons: ['Close']
    });
    alert.present();
  }

  isOKfunction() {
    var method: string = 'IsOk';
    var parameters: {}[] = [];
    let that = this;
    if (localStorage.getItem("StartInterval") != null) {
      this.url.forExtraSecurity(function (err, resp) {
        if (resp) {
          localStorage.setItem("newtm", resp)
          var encr = new Encrypt.JSEncrypt();
          var encrUser = encr.getAddition1(localStorage.getItem("userId"));
          that.userId = '';
          var uid = encrUser;
          that.storage.get(TOKEN_KEY).then((res) => {
            if (res) {
              that.checkisOK(parameters, method, that, uid, res);
            }
          })
        } else {
          that.url.stopLoadingToast();
        }
      });
    }
  }

  private static isOkParams(userId, sessionid): {}[] {
    var parameters: {}[] = [];
    parameters["User_Id"] = userId;
    parameters["session_id"] = sessionid;
    return parameters;
  }

  checkisOK(parameters, method, that, uid, sessionid) {
    parameters['IsOk xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HomePage.isOkParams(uid, sessionid);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    this.httpSoap.testService(parameters, function (err, response) {
      if (response) {
        var trip = response;
        var s = trip['SOAP:Envelope'];
        if (s['SOAP:Body'][0]['SOAP:Fault']) {
          that.callLogout();
          let toast = that.toastCtrl.create({
            message: "Session expired",
            duration: 2000,
            position: "top"
          });
          toast.present();
          that.url.stopLoading();
        }
      } else {
        that.url.stopLoading();
      }
    })
  }

  changePass() {
    let profileModal = this.modalCtrl.create('ChangePasswordPage', {
      param: 'byuser'
    });
    profileModal.present();
  }

  doRefresh(refresher) {
    console.log("On refresh: ", Math.floor(new Date().getTime() / 1000.0))
    let that = this;
    this.url.startLoadingToast().present();
    this.url.checkSession(function (err, resp) {
      if (resp) {
        // that.url.stopLoading();
        that.tk = undefined;
        that.tk = localStorage.getItem("gfdsa");
        localStorage.setItem("newtm", resp)
        that.getPOcount();
      } else {
        console.log("not getting response becz of err: ", err)
        that.toastCtrl.create({
          message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
          position: 'bottom',
          duration: 3000
        }).present();
        that.url.stopLoadingToast();
      }
    })
    setTimeout(() => {
      refresher.complete();
    }, 100);
  }

  getPOcount() {
    var method: string = 'GetPendingCounts';
    var parameters: {}[] = [];
    let that = this;
    // localStorage.setItem("newtm", resp)
    var encr = new Encrypt.JSEncrypt();
    var encrUser = encr.getAddition1(localStorage.getItem("userId"));
    that.userId = '';
    that.userId = encrUser;
    that.getPOsubCode(parameters, method, that);
  }
  private static userPOcount(username, tk): {}[] {
    var parameters: {}[] = [];
    parameters["userId"] = username;
    parameters["tk"] = tk;
    return parameters;
  }

  envelopeBuilder(requestBody: string): string {
    return "<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP:Body>" +
      requestBody +
      "</SOAP:Body>" +
      "</SOAP:Envelope>";
  }

  polist() {
    // debugger
    let that = this;
    console.log(this.url.loadingToast);
    if (this.url.loadingToast) {
      this.url.stopLoadingToast();
    }
    // that.url.stopLoadingToast();
    this.navCtrl.push('PoListPage', {
      param: this.userId,
      count: that.count
    })
  }

  prlist() {
    // debugger
    let that = this;
    if (this.url.loadingToast) {
      this.url.stopLoadingToast();
    }
    // that.url.stopLoadingToast();
    this.navCtrl.push('PRListPage', {
      param: this.userId,
      count: that.count1
    })
  }

  callLogout() {
    var method: string = 'LogoutAuthUser';
    var parameters: {}[] = [];
    let that = this;

    var encr = new Encrypt.JSEncrypt();
    var encrUser = encr.getAddition1(localStorage.getItem("userId"));
    var uId = encrUser;
    that.logoutFunc(parameters, method, that, uId);
  }

  private static logoutParams(username): {}[] {
    var parameters: {}[] = [];
    parameters["USER_ID"] = username;
    return parameters;
  }

  logoutFunc(parameters, method, that, uId) {
    parameters['LogoutAuthUser xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HomePage.logoutParams(uId);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };

    that.navCtrl.setRoot("LoginPage");
    this.httpSoap.testService(parameters, function (err, response) {

      ///////// delete all temp(cache) files from device //////
      // debugger
      // var directoryEntry = that.file.externalApplicationStorageDirectory;
      // that.file.getFile(directoryEntry, 'statement.pdf', flags)
      // that.file.removeFile(path, fileName);
      // that.file.listDir(that.file.cacheDirectory, 'io.smartapprovalsiOS.com').then((result) => {

      //   console.log('files in io.smartapprovalsiOS.com directory: ', JSON.stringify(result));

      //   console.log('Started deleting files from cache folder!');

      //   for (let file of result) {

      //     if (file.isFile == true) {

      //       that.file.removeFile(that.file.cacheDirectory + '/io.smartapprovalsiOS.com/', file.name).then(data => {
      //         console.log('file removed: ', that.file);
      //         data.fileRemoved.getMetadata(function (metadata) {
      //           let name = data.fileRemoved.name;
      //           let size = metadata.size;
      //           let fullPath = data.fileRemoved.fullPath;
      //           console.log('Deleted file: ', name, size, fullPath);
      //           console.log('Name: ' + name + ' / Size: ' + size);
      //         });
      //       }).catch(error => {
      //         file.getMetadata(function (metadata) {
      //           let name = file.name;
      //           let size = metadata.size;
      //           console.log('Error deleting file from cache folder: ', error);
      //           console.log('Name: ' + name + ' / Size: ' + size);
      //         });
      //       });

      //     }
      //   }
      // });
      //////// end file cleaning /////
      if (response) {

        var inID = JSON.parse(localStorage.getItem("intervalID"));
        clearInterval(inID);
        localStorage.removeItem("intervalID");
        localStorage.removeItem("LOGGEDIN");
        localStorage.removeItem("oldPass");
        localStorage.removeItem("userId");
        localStorage.removeItem("userName");
        localStorage.removeItem("userdetail");
        localStorage.removeItem("trewq");
        localStorage.removeItem("gfdsa");
        localStorage.removeItem("newtm");
        localStorage.removeItem("StartInterval");
        that.storage.remove("session_token");
        // that.navCtrl.setRoot("LoginPage");
      } else {
        console.log("not getting response becz of err: ", err)
        var inID = JSON.parse(localStorage.getItem("intervalID"));
        clearInterval(inID);
        localStorage.removeItem("intervalID");
        localStorage.removeItem("LOGGEDIN");
        localStorage.removeItem("oldPass");
        localStorage.removeItem("userId");
        localStorage.removeItem("userName");
        localStorage.removeItem("userdetail");
        localStorage.removeItem("trewq");
        localStorage.removeItem("gfdsa");
        localStorage.removeItem("newtm");
        localStorage.removeItem("StartInterval");
        that.storage.remove("session_token");
        that.url.stopLoading();
      }
    })
  }

  logout() {
    let alert = this.alertCtrl.create({
      message: "Do you want to exit Smart Approvals?",
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.callLogout();
        }
      },
      {
        text: "Back"
      }]
    })
    alert.present();
  }

  gotoFunction() {
    if (this.str[1] == "null" || this.str[1] == "N") {
      this.tk = localStorage.getItem("gfdsa");
      if (localStorage.getItem("PasswordChnaged") == null) {
        this.url.stopLoadingToast();
        let profileModal = this.modalCtrl.create('ChangePasswordPage');
        profileModal.present();
      } else {
        if (localStorage.getItem("PasswordChnaged") == "PasswordChnaged") {
          this.userId = localStorage.getItem("userId");
          this.getPOcount()
        }
      }
    } else {
      if (this.str[1] == "Y") {
        this.tk = localStorage.getItem("gfdsa");
        this.userId = localStorage.getItem("userId");
        this.getPOcount();
      }
    }
  }

  getPOsubCode(parameters, method, that) {
    parameters['GetPendingCounts xmlns="http://schemas.cordys.com/SterliteDatabaseMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HomePage.userPOcount(that.userId, that.tk);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    // that.url.startLoading().present();
    console.log("before GetPendingCounts: ", Math.floor(new Date().getTime() / 1000.0))
    this.httpSoap.testService(parameters, function (err, response) {
      console.log("after GetPendingCounts: ", Math.floor(new Date().getTime() / 1000.0))
      if (response) {
        that.url.stopLoadingToast();
        // debugger;
        var trip = response;
        var s = trip['SOAP:Envelope'];
        if (s['SOAP:Body'] == undefined) {
          // that.url.startLoading();
          let alert = that.alertCtrl.create({
            message: 'Something went wrong. Please visit after some time.',
            buttons: [{
              text: 'Okay'
            }]
          });
          alert.present();
        } else {
          try {
            console.log("before render: ", Math.floor(new Date().getTime() / 1000.0))
            var a = s['SOAP:Body'][0].GetPendingCountsResponse;
            var b = a[0].tuple[0].old[0].getPendingCounts[0].getPendingCounts[0];
            var split1 = b.split("#");
            var po = split1[0].split(":");
            that.count = po[1];
            var pr = split1[1].split(":");
            that.count1 = pr[1];
            console.log("after render: ", Math.floor(new Date().getTime() / 1000.0))
            // that.count = b;
            // that.getPRcount();
          }
          catch (e) {
            var a = s['SOAP:Body'][0]['SOAP:Fault'];
            var b = a[0].faultstring[0]._;
            console.log("catch token expired: ", b)
            if (b == 'EXPIRED_SESSION') {
              that.callLogout();
            } else {
              let toast = that.toastCtrl.create({
                message: "Token expired!! Please refresh once..",
                duration: 2500,
                position: "top"
              });
              toast.present();
            }
          }

        }
      } else {
        that.url.stopLoadingToast();
      }
    })
  }

}