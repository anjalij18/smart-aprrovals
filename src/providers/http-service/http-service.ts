import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import { LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as Encrypt from 'jsencrypt';
import { HttpSoapProvider } from '../http-soap/http-soap';
import { URLS } from '../urls';

const TOKEN_KEY = 'session_token';

@Injectable()
export class HttpServiceProvider {
  private headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
  loading: any;
  servicePort: any = '';
  timestamp: any;
  userId: any;
  loadingToast: any;
  servicePath: any;
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public httpSoap: HttpSoapProvider,
    public toastCtrl: ToastController,
    public urls: URLS,
  ) {
    console.log('Hello HttpServiceProvider Provider');
    this.servicePort = this.servicePort;
    this.servicePath = this.urls._baseURL;
  }

  startLoading() {
    return this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
  }

  stopLoading() {
    return this.loading.dismiss();
  }

  startLoadingToast() {
    return this.loadingToast = this.toastCtrl.create({
      message: "Please wait while the content is loading...",
      position: "bottom"
    });
  }

  stopLoadingToast() {
    return this.loadingToast.dismiss();
  }

  servicePathUrl() {
    return this.servicePath;
  }

  servicePortUrl() {
    return this.servicePort;
  }

  approvepoCall(taskid) {
    return this.http.get(this.servicePort + 'http://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve', { headers: this.headers })
    // return this.http.get(this.servicePort + 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Approve', { headers: this.headers })
      .map(res => res);
  }

  rejectCall(taskid) {
    // return this.http.get(this.servicePort + 'https://1dot.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject', { headers: this.headers })
    return this.http.get(this.servicePort + 'http://1dottest.sterliteapps.com/cordys/wcp/library/util/eventservice/com.cordys.approval.TaskApproval.wcp?TaskID=' + taskid + '&PRNumber=PO%20Number%20Awaiting%20for%20your%20Release&status=Reject', { headers: this.headers })
      .map(res => res)
  }

  checkSession(cb: any): void {
    var parameters: {}[] = [];
    parameters['GetCurrentTimeStamp xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HttpServiceProvider.userSessionPara();
    parameters = [{ "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } }];
    let that = this;
    this.httpSoap.testService(parameters[0], function (err, response) {
      if (response) {
        var trip = response;
        var s = trip['SOAP:Envelope'];
        try {
          var a = s['SOAP:Body'][0].GetCurrentTimeStampResponse;
          var b = a[0].tuple[0].old[0].getCurrentTimeStamp[0].getCurrentTimeStamp[0]
          that.timestamp = b;
          localStorage.setItem("trewq", that.timestamp);
          that.storage.set("trewq", that.timestamp);
          that.storage.get(TOKEN_KEY).then((res) => {
            if (res) {
              that.userId = localStorage.getItem("userId");
              var a = new Encrypt.JSEncrypt();  // called third party js library for secury purpose
              var strstr = that.userId + "$$$" + res;
              a.getAddition(strstr);
              cb(err, that.timestamp);
            }
          })
        }
        catch (e) {
          if (that.loading) {
            that.loading.dismiss();
          }
          if (that.loadingToast) {
            that.loadingToast.dismiss();
          }
          var a = s['SOAP:Body'][0]['SOAP:Fault'];
          var b = a[0].faultstring[0]._;
          let toast = that.toastCtrl.create({
            message: "Something went wrong. Please try again in some time.",
            duration: 3000,
            position: "top"
          });
          toast.present();
        }
      } else {
        cb(err, that.timestamp);
      }
    })
  }

  forExtraSecurity(cb: any): void {
    var parameters: {}[] = [];
    parameters['GetCurrentTimeStamp xmlns="http://schemas.cordys.com/SterliteSyncModuleDBMetadata" preserveSpace="no" qAccess="0" qValues=""'] = HttpServiceProvider.userSessionPara();
    parameters = [{ "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } }];
    let that = this;
    // debugger
    this.httpSoap.testService(parameters[0], function (err, response) {
      if (response) {
        var trip = response;
        var s = trip['SOAP:Envelope'];
        try {
          var a = s['SOAP:Body'][0].GetCurrentTimeStampResponse;
          var b = a[0].tuple[0].old[0].getCurrentTimeStamp[0].getCurrentTimeStamp[0];
          that.timestamp = b;
          cb(err, that.timestamp);
        }
        catch (e) {
          if (that.loading) {
            that.loading.dismiss();
          }
          if (that.loadingToast) {
            that.loadingToast.dismiss();
          }
          var a = s['SOAP:Body'][0]['SOAP:Fault'];
          var b = a[0].faultstring[0]._;
          // console.log("catch token expired: ", b)
          let toast = that.toastCtrl.create({
            message: "Something went wrong. Please try again in some time.",
            duration: 3000,
            position: "top"
          });
          toast.present();
        }
      } else {
        cb(err, response);
      }
    })
  }

  private static userSessionPara(): {}[] {
    var parameters: any;
    parameters = "";
    return parameters;
  }
}
